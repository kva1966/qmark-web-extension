/*
 * Tests - other dependencies (including app and libs) recursively inferred
 *   from test imports.
 */
require.context('./');


/*
 * Dynamic Test Loading because Mocha is Moronic
 */

// All test files are bundled as modules, the mocha runner by default cannot import
// and execute them -- it is not module-aware for the 'browser' target.

// So we either explicitly import each test:
// import * as IndexerTest from './indexer.test';
// Or generate static import statements dynamically, and load that file.

require('./generated-module-imports');

// Using the dynamic import() statement does not work since mocha is running
// at the top level, while import() produces an async promise
