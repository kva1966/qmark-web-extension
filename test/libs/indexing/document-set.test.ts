import * as Mocha from 'mocha';
import { assert } from 'chai';

import { Document, DocumentSet, ID } from 'app-libs/indexing/document-set';

class TestDoc implements Document {
  readonly id: ID;

  constructor(id: ID) {
    this.id = id;
  }
}

describe('DocumentSet', () => {
  let doc = (id: ID): TestDoc => {
    return new TestDoc(id);
  }

  let docSetOf = (...docs: TestDoc[]): DocumentSet<TestDoc> => {
    return new DocumentSet<TestDoc>(docs);
  }

  describe('initial state of empty set', () => {
    it('empty docset created', () => {
      let docs = docSetOf();

      assert.strictEqual(docs.size(), 0);
      assert.strictEqual(docs.isEmpty(), true);
    });
  });

  describe('add(doc) and has(doc)', () => {
    it('add single', () => {
      let docs = docSetOf(doc('1'));

      assert.strictEqual(docs.has(doc('1')), true);
      assert.strictEqual(docs.size(), 1);
      assert.strictEqual(docs.isEmpty(), false); // sanity
    });

    it('add multiple', () => {
      let docs = docSetOf(doc('1'), doc('2'));

      assert.strictEqual(docs.has(doc('1')), true);
      assert.strictEqual(docs.has(doc('2')), true);
      assert.strictEqual(docs.size(), 2);
    });

    it('docs with duplicate ids are not added', () => {
      let docs = docSetOf(doc('1'), doc('1'), doc('3'));

      assert.strictEqual(docs.has(doc('1')), true);
      assert.strictEqual(docs.has(doc('3')), true);
      assert.strictEqual(docs.size(), 2);
    });
  });

  describe('equals(other)', () => {
    it ('empty sets are equal', () => {
      assert.strictEqual(docSetOf().equals(docSetOf()), true);
    });

    it ('equal irrespective of order', () => {
      let docs: DocumentSet<TestDoc> = docSetOf(doc('1'), doc('2'));
      
      assert.strictEqual(docs.equals(docSetOf(doc('1'), doc('2'))), true);
      assert.strictEqual(docs.equals(docSetOf(doc('2'), doc('1'))), true);
    });

    it ('empty vs. non-empty are never equal', () => {
      assert.strictEqual(docSetOf().equals(docSetOf(doc('1'))), false);
      assert.strictEqual(docSetOf(doc('1')).equals(docSetOf()), false);
    });

    it ('differing size sets are never equal', () => {
      assert.strictEqual(
        docSetOf(doc('1')).equals(docSetOf(doc('1'), doc('2'))), 
        false
      );

      assert.strictEqual(
        docSetOf(doc('1'), doc('2')).equals(docSetOf(doc('1'))), 
        false
      );
    });
  });


  describe('delete(doc) and has(doc)', () => {
    it('deleted found docs', () => {
      let docs = docSetOf(doc('1'), doc('10'), doc('66'));

      // initial state
      assert.strictEqual(docs.has(doc('1')), true);
      assert.strictEqual(docs.has(doc('10')), true);
      assert.strictEqual(docs.has(doc('66')), true);
      assert.strictEqual(docs.isEmpty(), false);
      assert.strictEqual(docs.size(), 3);

      // delete
      assert.strictEqual(docs.delete(doc('1')), true);
      assert.strictEqual(docs.delete(doc('66')), true);

      // postconditions
      assert.strictEqual(docs.has(doc('1')), false);
      assert.strictEqual(docs.has(doc('10')), true);
      assert.strictEqual(docs.has(doc('66')), false);
      assert.strictEqual(docs.size(), 1);
      assert.strictEqual(docs.isEmpty(), false);
    });

    it('no docs found to delete', () => {
      let docs = docSetOf(doc('1'), doc('2'));

      // initial state
      assert.strictEqual(docs.has(doc('1')), true);
      assert.strictEqual(docs.has(doc('2')), true);
      assert.strictEqual(docs.isEmpty(), false);
      assert.strictEqual(docs.size(), 2);

      // delete
      assert.strictEqual(docs.delete(doc('3')), false);
      assert.strictEqual(docs.delete(doc('3')), false);
      assert.strictEqual(docs.delete(doc('20')), false);

      // postconditions - no change
      assert.strictEqual(docs.has(doc('1')), true);
      assert.strictEqual(docs.has(doc('2')), true);
      assert.strictEqual(docs.isEmpty(), false);
      assert.strictEqual(docs.size(), 2);
    });
  });

  describe('intersection(other)', () => {
    it('no intersection', () => {
      let docs1 = docSetOf(doc('1'));
      let docs2 = docSetOf(doc('2'));

      assert.strictEqual(docs1.intersection(docs2).isEmpty(), true);
    });

    it('has intersection', () => {
      let docs1 = docSetOf(doc('1'), doc('3'));
      let docs2 = docSetOf(doc('2'), doc('1'), doc('3'));
      let intersection = docs1.intersection(docs2);

      assert.strictEqual(intersection.has(doc('1')), true);
      assert.strictEqual(intersection.has(doc('3')), true);
      assert.strictEqual(intersection.size(), 2);
      assert.strictEqual(
        intersection.equals(docSetOf(doc('1'), doc('3'))),
        true
      );
    });
  });

  describe('filter(fn)', () => {
    it('found documents', () => {
      let docs: DocumentSet<TestDoc> = docSetOf(doc('1'), doc('22'), doc('44'), doc('55'));
      let results: TestDoc[] = docs.filter((doc: TestDoc) => {
        return doc.id === '22' || doc.id === '1';
      })
      let resultDocSet = new DocumentSet<TestDoc>(results);

      assert.strictEqual(results.length, 2);
      assert.strictEqual(resultDocSet.size(), 2);
      assert.strictEqual(resultDocSet.has(doc('1')), true);
      assert.strictEqual(resultDocSet.has(doc('22')), true);
    });

    it('no documents found returns empty array ', () => {
      let docs: DocumentSet<TestDoc> = docSetOf(doc('1'), doc('22'), doc('44'), doc('55'));
      let results: TestDoc[] = docs.filter((doc: TestDoc) => {
        return doc.id === '101';
      })

      assert.strictEqual(results.length, 0);
    });
  });

  describe('getAll()', () => {
    it ('all documents in set returned', () => {
      let docs: DocumentSet<TestDoc> = docSetOf(doc('1'), doc('22'), doc('44'), doc('55'));
      
      let all = new DocumentSet<TestDoc>(docs.getAll());

      assert.strictEqual(docs.equals(new DocumentSet<TestDoc>(docs.getAll())), true);
    });
  });

});
