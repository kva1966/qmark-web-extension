import * as Mocha from 'mocha';
import { assert } from 'chai';

import { NGram, NGramSplitter } from 'app-libs/indexing/ngram';


describe('NGramSplitter', () => {
  describe('split()', () => {
    describe('bad N values', () => {
      it('0 N value', () => {
        assert.throws(() => { new NGramSplitter(0) }, Error, "Bad N value for n-gram")
      });

      it('Negative N value', () => {
        assert.throws(() => { new NGramSplitter(-1) }, Error, "Bad N value for n-gram")
      });
    });

    describe('1-gram', () => {
      let splitter = new NGramSplitter(1);

      it('Empty String -> []', () => {
        assert.deepEqual(splitter.split(""), []);
      });

      it('U -> u', () => {
        assert.deepEqual(splitter.split("U"), ["u"]);
      });

      it('Un -> U, n', () => {
        assert.deepEqual(splitter.split("Un"), ['u', 'n']);
      });

      it('UnI -> u, n, i', () => {
        assert.deepEqual(splitter.split("UnI"), ['u', 'n', 'i']);
      });

      it('U n   I -> u, n, i', () => {
        assert.deepEqual(splitter.split("U   n  I\n\tx"), ['u', 'n', 'i', 'x']);
      });
    });

    describe('2-gram', () => {
      let splitter = new NGramSplitter(2);

      it('Empty String -> []', () => {
        assert.deepEqual(splitter.split(""), []);
      });

      it('U -> []', () => {
        assert.deepEqual(splitter.split("U"), []);
      });

      it('Un -> un', () => {
        assert.deepEqual(splitter.split("Un"), ['un']);
      });

      it('Uni -> un, ni', () => {
        assert.deepEqual(splitter.split("Uni"), ['un', 'ni']);
      });

      it('Unix -> un, ni, ix', () => {
        assert.deepEqual(splitter.split("Unix"), ['un', 'ni', 'ix']);
      });

      it('U   n i   x\t\n -> un, ni, ix', () => {
        assert.deepEqual(splitter.split("U   n i   x\t\n"), ['un', 'ni', 'ix']);
      });
    });

    describe('3-gram', () => {
      let splitter = new NGramSplitter(3);

      it('Empty String -> []', () => {
        assert.deepEqual(splitter.split(""), []);
      });

      it('Un -> []', () => {
        assert.deepEqual(splitter.split("Un"), []);
      });

      it('Uni -> uni', () => {
        assert.deepEqual(splitter.split("Uni"), ['uni']);
      });

      it('Unix -> uni, nix', () => {
        assert.deepEqual(splitter.split("Unix"), ['uni', 'nix']);
      });

      it('Unixe -> uni, nix, ixe', () => {
        assert.deepEqual(splitter.split("Unixe"), ['uni', 'nix', 'ixe']);
      });

      it('   U \t\n nix   e -> uni, nix, ixe', () => {
        assert.deepEqual(splitter.split("   U \t\n nix   e "), ['uni', 'nix', 'ixe']);
      });
    });

  }); // END split()

});

