import * as Mocha from 'mocha';
import { assert } from 'chai';

import { NGram } from 'app-libs/indexing/ngram';
import { InvertedIndex } from 'app-libs/indexing/inverted-index';
import { Document, DocumentSet, ID } from 'app-libs/indexing/document-set';

class TestDoc implements Document {
  readonly id: ID;

  constructor(id: ID) {
    this.id = id;
  }
}

describe('InvertedIndex', () => {
  let doc = (id: ID): TestDoc => {
    return new TestDoc(id);
  }

  let docSetOf = (...docs: TestDoc[]): DocumentSet<TestDoc> => {
    return new DocumentSet<TestDoc>(docs);
  }

  let emptyIndex = (): InvertedIndex<TestDoc> => { return new InvertedIndex(); }

  let indexOf = (objects: {ngram: NGram, docs: TestDoc[]}[]) => {
    return InvertedIndex.of(objects);
  };

  describe('addDocument[s](ngram, doc)', () => {
    it('add one', () => {
      let index: InvertedIndex<TestDoc> = emptyIndex();

      index.addDocument("ob", doc('bob'));

      assert.strictEqual(index.has("non-existent"), false);
      assert.strictEqual(index.has("ob"), true);
      assert.strictEqual(index.getDocumentSet("ob").has(doc('bob')), true);
    });

    it('add multiple', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      assert.strictEqual(index.has("un"), true);
      assert.strictEqual(index.getDocumentSet("un").equals(docSetOf(doc('unix'), doc('bun'))), true);

      assert.strictEqual(index.has("ni"), true);
      assert.strictEqual(index.getDocumentSet("ni").equals(docSetOf(doc('unix'), doc('nitrate'))), true);
    });

    it('add duplicate has no effect', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      index.addDocument("un", doc('unix'));
      assert.strictEqual(index.has("un"), true);
      assert.strictEqual(index.getDocumentSet("un").size(), 2);
      assert.strictEqual(index.getDocumentSet("un").equals(docSetOf(doc('unix'), doc('bun'))), true);
    });
  });

  describe('removeDocument(ngram, doc)', () => {
    it('removed successfully', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      index.removeDocument("un", doc('unix'));

      assert.strictEqual(index.has("un"), true);
      assert.strictEqual(index.getDocumentSet("un").size(), 1);
      assert.strictEqual(index.getDocumentSet("un").equals(docSetOf(doc('bun'))), true);
      // no change to unaffected ngram, client must be sure to update the index for every ngram.
      assert.strictEqual(index.has("ni"), true);
      assert.strictEqual(index.getDocumentSet("ni").equals(docSetOf(doc('unix'), doc('nitrate'))), true);      
    });

    it('removing non-existent has no effect', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      index.removeDocument("un", doc('foobario'));
      index.removeDocument("non-existent key", doc('dont-care'));
      
      assert.strictEqual(index.has("un"), true);
      assert.strictEqual(index.getDocumentSet("un").equals(docSetOf(doc('unix'), doc('bun'))), true);
      assert.strictEqual(index.has("ni"), true);
      assert.strictEqual(index.getDocumentSet("ni").equals(docSetOf(doc('unix'), doc('nitrate'))), true);

    });
  });

  describe('removeDocumentCompletely(doc)', () => {
    it('removed successfully', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      index.removeDocumentCompletely(doc('unix'));

      assert.strictEqual(index.getDocumentSet("un").size(), 1, "index[un] has size 1");
      assert.strictEqual(index.getDocumentSet("un").equals(docSetOf(doc('bun'))), true, "only doc(bun) remaining");
      // no change to unaffected ngram, client must be sure to update the index for every ngram.
      assert.strictEqual(index.has("ni"), true);
      assert.strictEqual(index.getDocumentSet("ni").size(), 1, "index[ni] has size one");
      assert.strictEqual(index.getDocumentSet("ni").equals(docSetOf(doc('nitrate'))), true, "only doc(nitrate) remaining");      
    });

    it('removing non-existent has no effect', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      index.removeDocumentCompletely(doc('non-existent'));
      
      assert.strictEqual(index.has("un"), true);
      assert.strictEqual(index.getDocumentSet("un").equals(docSetOf(doc('unix'), doc('bun'))), true);
      assert.strictEqual(index.has("ni"), true);
      assert.strictEqual(index.getDocumentSet("ni").equals(docSetOf(doc('unix'), doc('nitrate'))), true);
    });
  });

  describe('getDocumenSet(ngram)', () => {
    it('always creates a copy of the document set', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      // get
      let docSet = index.getDocumentSet("un");
      assert.strictEqual(docSet.equals(docSetOf(doc('unix'), doc('bun'))), true);
      
      // manipulate obtained set.
      docSet.delete(doc('unix'));
      assert.strictEqual(docSet.equals(docSetOf(doc('bun'))), true);

      // get again, should give original set's contents
      let docSet2 = index.getDocumentSet("un");
      assert.strictEqual(docSet2.equals(docSetOf(doc('unix'), doc('bun'))), true);
    });

    it('returns empty set on non-existent ngram', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      assert.strictEqual(index.getDocumentSet("non-existent").isEmpty(), true);
    });
  });

  describe('getAllDocuments()', () => {
    it('retrieves a document set of all unique documents', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      // get
      let docSet = index.getAllDocuments();
      assert.strictEqual(docSet.size(), 3);
      assert.strictEqual(docSet.equals(docSetOf(doc('unix'), doc('bun'), doc('nitrate'))), true);
    });
  });

  describe('search(phrase, ngramLen, substringSearch)', () => {
    it('no results - non-existent ngram', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      // get
      let docSet = index.search('foo', 2);
      assert.strictEqual(docSet.isEmpty(), true);
    });

    it('no results - no intersection', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      // get
      let docSet = index.search('cun', 2);
      assert.strictEqual(docSet.isEmpty(), true);
    });

    it('found results - single intersection', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate')] }
      ]);

      // get
      let docSet = index.search('uni', 2);
      assert.strictEqual(docSet.equals(docSetOf(doc('unix'))), true);
    });

    it('found results - multiple intersections', () => {
      let index: InvertedIndex<TestDoc> = indexOf([
        { ngram: "un", docs: [doc('unix'), doc('bun')] },
        { ngram: "ni", docs: [doc('unix'), doc('nitrate'), doc('initial')] },
        { ngram: "it", docs: [doc('nitrate'), doc('italic'), doc('initial')] },
        { ngram: "tr", docs: [doc('trax'), doc('nitrate')] }
      ]);

      // get
      let docSet = index.search('nit', 2);
      assert.strictEqual(docSet.equals(docSetOf(doc('nitrate'), doc('initial'))), true);
    });
  });
});

