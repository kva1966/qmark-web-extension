import * as Mocha from 'mocha';
import { assert } from 'chai';

import Sett from 'app-libs/set';


describe('Sett', () => {
  let setOf = (values?: number[]): Sett<number> => {
    return new Sett<number>(values);
  }

  describe('isEmpty()', () => {
    it('empty', () => {
      assert.strictEqual(setOf().isEmpty(), true);
    });

    it('not empty', () => {
      assert.strictEqual(setOf([1]).isEmpty(), false);
    });
  });

  describe('difference|minus(other)', () => {
    it('empty set diffed with empty set yield another empty set', () => {
      let diff = setOf().difference(setOf());

      assert.strictEqual(diff.isEmpty(), true);
      assert.deepEqual(diff, setOf());
    });

    it('empty set diffed with non-empty yields the empty set', () => {
      let diff = setOf().difference(setOf([1, 2, 3]));
      
      assert.strictEqual(diff.isEmpty(), true);
      assert.deepEqual(diff, setOf());
    });

    it('non-empty set diffed with empty yields the non-empty set', () => {
      let diff = setOf([1, 2, 3]).difference(setOf());

      assert.deepEqual(diff, setOf([1, 2, 3]));
    });

    it('sets with the same content yield the empty set', () => {
      let diff = setOf([1, 2, 3]).difference(setOf([1, 2, 3]));

      assert.strictEqual(diff.isEmpty(), true);
      assert.deepEqual(diff, setOf());
    });

    it('sets with distinct content yield a new set with first set\'s contents', () => {
      let diff = setOf([1, 2, 3]).difference(setOf([4, 5, 6]));

      assert.deepEqual(diff, setOf([1, 2, 3]));
    });

    it('sets with some intersecting comments yield the first sets minus the intersecting elems', () => {
      let diff = setOf([8, 1, 2, 3]).difference(setOf([4, 5, 6, 1, 2]));

      assert.deepEqual(diff, setOf([8, 3]));
    });
  });

  describe("equals(other)", () => {
    it("empty sets are equal", () => {
      assert.strictEqual(setOf().equals(setOf()), true);
    });

    it("sets with exactly the same values are equal, irrespective of order", () => {
      // exhaustive perms
      assert.strictEqual(setOf([1, 2]).equals(setOf([1, 2])), true);
      assert.strictEqual(setOf([1, 2]).equals(setOf([2, 1])), true);
      assert.strictEqual(setOf([2, 1]).equals(setOf([1, 2])), true);

      // random perms
      assert.strictEqual(setOf([1, 2, 3]).equals(setOf([3, 2, 1])), true);
      assert.strictEqual(setOf([2, 1, 3]).equals(setOf([3, 2, 1])), true);
      assert.strictEqual(setOf([2, 3, 1]).equals(setOf([3, 2, 1])), true);
      assert.strictEqual(setOf([3, 1, 2]).equals(setOf([3, 2, 1])), true);
    });

    it("empty and populated sets are never equal", () => {
      assert.strictEqual(setOf([1]).equals(setOf()), false);
      assert.strictEqual(setOf().equals(setOf([1])), false);
    });

    it("completely disjoint sets are never equal", () => {
      assert.strictEqual(setOf([1]).equals(setOf([2])), false);
      assert.strictEqual(setOf([2]).equals(setOf([1])), false);
    });

    it("sets with at least one differing value are never equal", () => {
      assert.strictEqual(setOf([1, 2]).equals(setOf([1, 6])), false);
      assert.strictEqual(setOf([1, 6]).equals(setOf([1, 2])), false);
      assert.strictEqual(setOf([6, 1]).equals(setOf([7, 1])), false);
      assert.strictEqual(setOf([2, 1]).equals(setOf([2, 1, 6])), false);
      assert.strictEqual(setOf([2, 1, 6]).equals(setOf([2, 1])), false);
    });
  });

  describe("union(other)", () => {
    it("empty sets yield the empty set", () => {
      assert.deepEqual(setOf().union(setOf()), setOf());
    });

    it("equal sets yield a new set with the same values", () => {
      assert.deepEqual(setOf([1, 2, 3]).union(setOf([3, 2, 1])), setOf([1, 2, 3]));
    });

    it("empty set with a non-empty set yields a set with the non-empty set's vals", () => {
      assert.deepEqual(setOf().union(setOf([3, 2, 1])), setOf([3, 2, 1]));
      assert.deepEqual(setOf([1, 2, 3]).union(setOf()), setOf([1, 2, 3]));
    });

    it("distinct sets yield a set with combined values", () => {
      assert.deepEqual(setOf([1, 2, 3]).union(setOf([4, 5, 6])), setOf([1, 2, 3, 4, 5, 6]));
    });

    it("sets with some intersecting values yield a set with those and any distinct vals", () => {
      assert.deepEqual(setOf([1, 2, 3, 0]).union(setOf([2, 3, 7])), setOf([1, 2, 3, 0, 7]));
    });
  });

  describe("intersection(other)", () => {
    it("empty sets yield the empty set", () => {
      assert.deepEqual(setOf().intersection(setOf()), setOf());
    });

    it("empty set with a non-empty set yields the empty set", () => {
      assert.deepEqual(setOf().intersection(setOf([1, 2, 3])), setOf());
      assert.deepEqual(setOf([1, 2, 3]).intersection(setOf()), setOf());
    });

    it("equal sets yield a new set with the same values", () => {
      assert.deepEqual(setOf([1, 2, 3]).intersection(setOf([3, 2, 1])), setOf([1, 2, 3]));
    });

    it("completely disjoint sets yield the empty set", () => {
      assert.deepEqual(setOf([6, 7, 8]).intersection(setOf([1, 2, 3])), setOf());
    });

    it("sets with common values yield a new set with only those values", () => {
      assert.deepEqual(setOf([6, 7]).intersection(setOf([6, 2, 7])), setOf([6, 7]));
      assert.deepEqual(setOf([6, 7, 9, 3]).intersection(setOf([3, 9, 1, 2])), setOf([9, 3]));
    });
  });

  describe("isSupersetOf(other)", () => {
    it("empty sets are supersets", () => {
      assert.strictEqual(setOf().isSupersetOf(setOf()), true);
    });

    it("non-empty set is always a superset of an empty set", () => {
      assert.strictEqual(setOf([1]).isSupersetOf(setOf()), true);
      assert.strictEqual(setOf([1, 2, 3]).isSupersetOf(setOf()), true);
    });

    it("empty set is never a superset of a non-empty set", () => {
      assert.strictEqual(setOf().isSupersetOf(setOf([1])), false);
      assert.strictEqual(setOf().isSupersetOf(setOf([1, 2, 3])), false);
    });

    it("disjoint sets are never supersets", () => {
      assert.strictEqual(setOf([1]).isSupersetOf(setOf([2])), false);
      assert.strictEqual(setOf([1, 2, 3]).isSupersetOf(setOf([4, 5, 6])), false);
    });

    it("equal sets are supersets of each other", () => {
      assert.strictEqual(setOf([1]).isSupersetOf(setOf([1])), true);
      assert.strictEqual(setOf([1, 2]).isSupersetOf(setOf([2, 1])), true);
      assert.strictEqual(setOf([1, 2, 3]).isSupersetOf(setOf([2, 1, 3])), true);
    });

    it("set that has ALL values of other is its superset", () => {
      assert.strictEqual(setOf([1, 2]).isSupersetOf(setOf([1])), true);
      assert.strictEqual(setOf([1, 2]).isSupersetOf(setOf([2])), true);
      assert.strictEqual(setOf([1, 2, 4, 7]).isSupersetOf(setOf([1])), true);
      assert.strictEqual(setOf([1, 2, 4, 7]).isSupersetOf(setOf([2, 4])), true);
    });

    it("set missing any values of other is NOT its superset", () => {
      assert.strictEqual(setOf([1]).isSupersetOf(setOf([3])), false);
      assert.strictEqual(setOf([1, 3]).isSupersetOf(setOf([1, 9])), false);
      assert.strictEqual(setOf([1, 2, 4, 7]).isSupersetOf(setOf([1, 2, 4, 7, 8])), false);
      assert.strictEqual(setOf([1, 2, 4, 7]).isSupersetOf(setOf([1, 2, 4, 6])), false);
      assert.strictEqual(setOf([1, 2, 4, 7]).isSupersetOf(setOf([1, 2, 9])), false);
    });
  });

});
