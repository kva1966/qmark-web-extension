const PATH = process.argv[2];
const PATH_PATTERN = new RegExp(`^${PATH}/`);
const VALID_EXTENSIONS = ['js', 'ts'];
const VALID_SUFFIXES = VALID_EXTENSIONS.map(function(ext) { return `.test.${ext}` });
const EXTENSION_PATTERN = (function() {
  // -> '\.p1|\.p2|..\.pN$'
  let s = '\.' + VALID_EXTENSIONS.join('|\.') + '$';
  return new RegExp(s);
}());

// https://www.npmjs.com/package/recursive-readdir
var recursive = require("recursive-readdir");
var path = require('path');

function excludeFun(file, stats) {
  if (stats.isDirectory()) {
    return false; // allow recurse, don't ignore
  }

  let baseName = path.basename(file);

  return !VALID_SUFFIXES.some(function(suffix) {
    return baseName.endsWith(suffix);
  });
}


recursive(PATH, [excludeFun], function (err, files) {
  // `files` is an array of absolute file paths
  var f = files
            // Remove Path Prefix
            .map(function(fpath) { return fpath.replace(PATH_PATTERN, '') })
            // Remove Extensions
            .map(function(fpath) { return fpath.replace(EXTENSION_PATTERN, '') })
            // Sort by name
            .sort(function(path1, path2) {
              return path1.toLowerCase().localeCompare(path2.toLowerCase());
            });
  
  // Dump out import statements for each module
  for (let i = 0; i < f.length; i++) {
    console.log(`import * as Test${i} from './${f[i]}'`);
  }
});
