const path = require('path');
const webpack = require('webpack');
const MinifyPlugin = require("babel-minify-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');

const config = {
  target: 'web',

  // Resolve within src so that copying excludes 'src'
  context: path.resolve(__dirname, './src'),

  entry: {
    // App
    'static-resources': './bundle-index-static.js',
    'background': './background/bundle-index.js',
    'bookmark-dialog': './ui/bookmark-dialog/bundle-index.js',

    // Test
    'test': '../test/bundle-index.js'
  },

  output: {
    filename: 'bundles/[name].js',
    path: path.resolve(__dirname, 'dist')
  },

  devtool: "source-map",

  devServer: {
    contentBase: './dist'
  },

  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".ts", ".tsx", ".js", ".json"],

    // Module resolution, so we don't use stupid relative paths on client modules
    // https://webpack.js.org/configuration/resolve/
    alias: {
      // NOTE:
      // Need to set appropriate paths in tsconfig.json as well for TypeScript
      // awareness
      'app-libs': path.resolve(__dirname, 'src/libs/'),
      'ui': path.resolve(__dirname, 'src/ui/'),
      'background': path.resolve(__dirname, 'src/background/'),
    },
  },

  module: {
    rules: [
      // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
      { test: /\.tsx?$/, loader: "awesome-typescript-loader" },

      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },

      /*
       * Manifest
       */
      { 
        test: /manifest\.json$/,  
        use: {
          loader: 'file-loader', 
          options: {
            name: '[name].[ext]'
          }
        }
      },


      /*
       * Other static assets -- slightly more verbose to flatten out deeper
       * hierarchies (e.g. jquery-ui assets from node modules) into the standard
       * dist/assets output.
       */
      { 
        test: /images\/.+?\.(png|jpg)$/,  
        use: {
          loader: 'file-loader', 
          options: {
            outputPath: 'assets/style/images/',
            name: '[name].[ext]'
          }
        }
      },

      { 
        test: /icons\/.+?\.(png|jpg)$/,  
        use: {
          loader: 'file-loader', 
          options: {
            outputPath: 'assets/style/icons/',
            name: '[name].[ext]'
          }
        }
      },

      { 
        test: /\.css$/,  
        use: {
          loader: 'file-loader', 
          options: {
            outputPath: 'assets/style/',
            name: '[name].[ext]'
          }
        }
      },

      // https://webpack.js.org/loaders/html-loader/#export-into-html-files
      { test: /\.hbs$/, loader: "handlebars-loader" }
    ]
  },

  plugins: [
    // https://stackoverflow.com/questions/28969861/managing-jquery-plugin-dependency-in-webpack
    // This makes jQuery globally available, sucks but required to get jquery plugins to work.
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      "window.jQuery": "jquery'"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      template: 'ui/bookmark-dialog/view/index.hbs',
      filename: 'ui/bookmark-dialog.html'
    }),
    // Prod
    new MinifyPlugin()
  ]
};

module.exports = config;
