# TODO

* Bookmark Move
* Folder Handling UI: search/create/delete/rename/move, new tab 'folder ops'
    * delete must warn! can delete all bookmarks in that folder!
* [low] Substring Search of folders

# DONE

* Full Reindex Button on UI
* Use more distinctive icons on browser, and for extension.
* Searching hints on UI
* Empty string bookmarks should be marked as (no title), except root!
* Index Update when Folders Deleted/Removed/Changed/Moved
* Handle Port disconnection and reconnection on both background and UI scripts
* Index in Progress Message Handling
* Bookmark Selection and Deselection UI
* Animate Already Selected
* Bookmark Creation: See if adding notes is part of the interface
* Bookmark Search - only to be done if FF search interface is intolerable
    * [ok] single search box
    * [ok] single display list box
    * [ok] middle click to open in new tab in the background
    * [ok] regular click to open link as active
    * [ok] UI -> tabs: bookmark and bookmark search
    * [ok] show which folder bookmark was found in
    * [ok] show URL for context
    * [ok] right click to open edit URL dialog, allow modifying Title and URL
    * [ok] Deleting bookmarks
