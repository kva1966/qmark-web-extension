# QMark Web Extension

Web Extension providing an alternative bookmarking UI. The Web Extensions API 
is supposed to be mostly portable across browsers, but only Firefox has been 
targeted right now.

Nearly completely written in [TypeScript](http://typescriptlang.org/), with 
[webpack](http://webpack.js.org/) to handle modularisation.


# What Problem Does It Solve?

## Past

A very particular use case of the author's: quickly bookmarking to multiple 
folders. The current focus is on quick creation and categorisation of a 
bookmark, thus:

* The extension provides a flattened view of folders.
* Allows the user to filter/search through this view
* And select one or more folders from the filter or unfiltered list.
* Create a bookmark entry in all selected folders in one step.

Searching for bookmarks through the Firefox UI is not too bad, so the need 
duplicate this functionality with no addition (e.g. indexing a part of the 
webpage the bookmark points to) seems pointless.

## Present

The author decided there was value in having a more unified bookmarking 
interface, thus the following facilities have also been added in a separate tab:

* Bookmark searching
* Listing a summary of found bookmark details in the search view --- including 
    URL, a searchable field!
* Opening found results in tabs
* Modifying results' details (URL and Title)
* Deleting found results

The idea is to provide an alternative,
hopefully better bookmarking interface than what Firefox provides.

For bulk operations like moving multiple bookmarks across folders, the Firefox
UI still wins. There are no plans to support sophisticated bulk operations at
this time, since major reshuffling does not happen very much.


## Future

It would be nice to also be able to add, delete, and rename folders from the
same searchable interface. Again, this happens less often, and so is deferred.

The flattened view sure beats going through the tree interface on the Firefox 
UI, while still allowing hierarchies for categorisation.


# Build

Install `npm` and `yarn` Then:

        yarn
        npm run build
        npm run test

`yarn run` may just work, needs to be tested.

Development:

        npm run dev

Look at other delegated scripts in `package.json`.

Output is in the the `dist` folder. Using the Firefox `web-ext` npm package, 
one can test the plugin with an clear-profile Firefox instance like so:

        npm run build # Creates dist directory
        cd dist
        web-ext run

To build the extension so it can be loaded from file:

        cd dist
        web-ext build

This produces `dist/web-ext-artifacts/qmark-$VERSION>.zip`.


# Build and Sign

See `build-and-sign.sh`.

You'll need appropriate signing keys.


# Status

Feature-complete for the author's main use-case. To be tested on a browser with 
a larger number of bookmarks (hundreds to thousands).


# Licence

Apache Software Licence (ASL v2).
