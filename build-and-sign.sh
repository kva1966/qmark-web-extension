#!/bin/bash

source ./env.sh

THINK_DURATION_SECS=7
TEST_BUNDLE="bundles/test.*"

printf "[RELEASE REMINDER]\n"
printf "* Be sure to enable minify in webpack.config.js\n"
printf "* Update versions in manifest.json\n"
printf "* Update versions in package.json\n"
printf "* Tag release to version\n"
printf "${THINK_DURATION_SECS} seconds to think about it...\n\n"
sleep ${THINK_DURATION_SECS}

printf "OK, building..."

rm -rf dist

npm run build \
  && (cd dist && \
    rm -rf $TEST_BUNDLE && \
    web-ext sign \
      -v \
      --api-key ${WEB_EXT_API_KEY} \
      --api-secret ${WEB_EXT_API_SECRET} \
    )
