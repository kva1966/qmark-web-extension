export default class Util {
  static readonly CTX_INDEXER_SERVICE: string = 'IndexerService';
  static readonly CTX_FOLDER_INDEXER: string = 'FolderIndexer';
  static readonly CTX_BOOKMARK_DLG: string = 'BookmarkDialog';

  static prop(obj: any, propName: string, defaultVal: string = "<NONE>"): any {
    return obj.hasOwnProperty(propName) ? obj[propName] : defaultVal;
  }

  static assertNever(x: never): never {
    throw new Error("Unexpected object: " + x);
  }

  static assertTrue(cond: boolean, message: string) {
    if (cond !== true) {
      throw new Error(message);
    }
  }

  static isNullOrUndefined(v: any | undefined): boolean {
    return v === null || v === undefined;
  }

  static isEmpty(s: string | undefined): boolean {
    return (Util.isNullOrUndefined(s) || s!.trim() === '');
  }

  static trimmedOrDefaultIfEmpty(s: string | undefined, defaultVal: string): string {
    return Util.isEmpty(s) ? defaultVal : s!.trim();
  }

  static log(ctx: string, msg: any, ...args: any[]): void {
    if (!args || args.length === 0) {
      console.log(`[${ctx}] ${msg}`);
    } else {      
      console.log(`[${ctx}] ${msg}`, ...args);
    }
  }

  static enumValuesOf(enumType: any): number[] {
    // https://stackoverflow.com/questions/21293063/how-to-programmatically-enumerate-an-enum-type-in-typescript-0-9-5
    const objValues = Object.keys(enumType).map(k => enumType[k]);
    return objValues.filter(v => typeof v === "number") as number[];
  }

  static logIndexerService(msg: any, ...args: any[]): void {
    Util.log(Util.CTX_INDEXER_SERVICE, msg, ...args);
  }

  static logFolderIndexer(msg: any, ...args: any[]): void {
    Util.log(Util.CTX_FOLDER_INDEXER, msg, ...args);
  }

  static logBookmarkDialog(msg: any, ...args: any[]): void {
    Util.log(Util.CTX_BOOKMARK_DLG, msg, ...args);
  }

  static logPortDisconnection(ctx: string, p: browser.runtime.Port) {
    Util.log(Util.CTX_BOOKMARK_DLG, Util.getDisconnectedPortMessage(p), p);
  }

  static getDisconnectedPortMessage(p: browser.runtime.Port): string {
    let portError: string | undefined = p.error ? p.error.message : undefined;
    let reason: string = portError ? 
      `with the error[${portError}]. You may need to re-open the bookmark popup, 
      or restart the browser.` : 'normally (peer initiated).';
    return `Port[${p.name}] Disconnected ${reason}`;
  }

  static queryActiveTab(fnActiveTabHandler: (tab: browser.tabs.Tab) => void) {
    browser
      .tabs
      .query({ active: true, currentWindow: true })
      .then((tabs) => {
        fnActiveTabHandler(tabs[0]);
      });
  }

  // https://davidwalsh.name/javascript-debounce-function
  static debounce(func: (ctx: any, ...args: any[]) => any, waitMs: number, immediate: boolean): (ctx: any, ...args: any[]) => any {
    var timeout: number | null | undefined;

    return function() {
      let context = this;
      let args = arguments;

      let later = function() {
        timeout = null;
        if (!immediate) {
          func.apply(context, args);
        }
      };

      let callNow = immediate && !timeout;

      clearTimeout(timeout!);
      timeout = setTimeout(later, waitMs);

      if (callNow) {
        func.apply(context, args);
      }
    }
  };
}
