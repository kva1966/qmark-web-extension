import Sett from 'app-libs/set';
import Util from 'app-libs/util';

export type ID = string;

/**
 * Abstract Document Type
 */
export interface Document {
  /**
   * IMPORTANT: Document equality will be based on this, and this alone.
   */
  readonly id: ID;
}

/**
 * Provides a higher-level facade to working with abstract document objects 
 * with the efficiency of a set. Internally maintains the ID to document 
 * mapping for Set equality semantics.
 */
export class DocumentSet<D extends Document> {
  private readonly documentMap: Map<ID, D> = new Map<ID, D>();
  private readonly documentIds: Sett<ID> = new Sett<ID>();

  /**
   * Shallow copy.
   */
  static from<D extends Document>(docSet: DocumentSet<D>): DocumentSet<D> {
    return new DocumentSet<D>(
      docSet.filter((d) => { return true })
    );
  }

  constructor(docs?: D[]) {
    if (Util.isNullOrUndefined(docs)) {
      return;
    }

    docs!.forEach(d => this.add(d));
  }

  add(doc: D): DocumentSet<D> {
    this.documentIds.add(doc.id);
    this.documentMap.set(doc.id, doc);

    return this;
  }

  addAll(docSet: DocumentSet<D>) {
    docSet.forEach((d) => {
      this.add(d);
    })
  }

  delete(doc: D): boolean {
    if (this.has(doc)) {
      this.documentIds.delete(doc.id);
      this.documentMap.delete(doc.id);

      return true;
    }

    return false;
  }

  forEach(fn: (doc: D) => void): void {
    this.documentIds.forEach((id: ID) => {
      fn(this.resolveOne(id));
    });
  }

  map<T>(fn: (doc: D) => T): T[] {
    let results: T[] = []

    this.forEach((doc: D) => {
      results.push(fn(doc));
    });

    return results;
  }

  filter(predFn: (doc: D) => boolean): D[] {
    let results: D[] = [];

    this.forEach((doc: D) => {
      if (predFn(doc) === true) {
        results.push(doc);
      }
    });

    return results;
  }

  getAll(): D[] {
    return this.filter((d) => { return true; });
  }

  intersection(other: DocumentSet<D>): DocumentSet<D> {
    let ids: Sett<ID> = this.documentIds.intersection(other.documentIds);
    return this.resolve(ids);
  }

  has(doc: D): boolean {
    return this.documentIds.has(doc.id);
  }

  equals(other: DocumentSet<D>): boolean {
    return this.documentIds.equals(other.documentIds);
  }

  isEmpty(): boolean {
    return this.documentIds.isEmpty();
  }

  size(): number {
    return this.documentIds.size;
  }

  private resolve(ids: Sett<ID>): DocumentSet<D> {
    let docs: D[] = ids.map((id: ID) => {
      return this.resolveOne(id);
    });

    return new DocumentSet<D>(docs);
  }

  private resolveOne(id: ID): D {
    return this.documentMap.get(id)!;
  }
}
