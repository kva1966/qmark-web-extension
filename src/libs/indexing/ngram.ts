import Util from 'app-libs/util';

export type NGram = string;

const WHITESPACE_PATTERN: RegExp = /\s+/g;

export class NGramSplitter {
  // N >= 1, integer
  private readonly n: number;

  constructor(n: number) {
    let tmp = parseInt(n.toString(), 10);
    Util.assertTrue(tmp >= 1, "Bad N value for n-gram");
    this.n = tmp;
  }

  /**
   * For N = 3
   * Un -> []
   * Uni -> [uni]
   * Unix -> [uni, nix]
   * Unixe -> [uni, nix, ixe], and so on.
   */
  split(text: string): NGram[] {
    let cleanedTxt: string = this.cleanUp(text);
    let results: NGram[] = [];

    let _split = (idx: number): NGram[] => {
      let txt = cleanedTxt.substring(idx);

      if (txt.length < this.n) {
        return results;
      } else {
        results.push(txt.substring(0, this.n));
        return _split(idx + 1);
      }
    }

    return _split(0);
  }

  private cleanUp(text: string): string {
    return text
            .toLowerCase()
            .replace(WHITESPACE_PATTERN, '');
  }
}

