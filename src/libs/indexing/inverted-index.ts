import { Document, DocumentSet, ID } from 'app-libs/indexing/document-set';
import AppConstants from 'app-libs/app-constants';
import { NGram, NGramSplitter} from 'app-libs/indexing/ngram';
import Util from 'app-libs/util';

/*
 * Indexes documents by their ngrams.
 */
export class InvertedIndex<D extends Document> {
  private readonly index: Map<NGram, DocumentSet<D>> = new Map<NGram, DocumentSet<D>>();

  static of<D extends Document>(objects: { ngram: NGram, docs: D[] }[]): InvertedIndex<D> {
    let index = new InvertedIndex<D>();

    for (let o of objects) {
      index.addDocuments(o.ngram, o.docs);
    }

    return index;
  }

  addDocument(ngram: NGram, doc: D): void {
    return this.lookup<void>(
      ngram,
      (docSet: DocumentSet<D>) => { docSet.add(doc); },
      () => { this.index.set(ngram, new DocumentSet<D>([doc])); }
    );
  }

  addDocuments(ngram: NGram, docs: D[]): void {
    for (let doc of docs) {
      this.addDocument(ngram, doc);
    }
  }

  removeDocument(ngram: NGram, doc: D): boolean {
    return this.lookup<boolean>(
      ngram,
      (docSet: DocumentSet<D>) => { return docSet.delete(doc); },
      () => { return false }
    );
  }

  /**
   * Possibly expensive operation, the document with the given ID is 
   * searched for across ALL ngrams, and removed. This is essentially used 
   * when a document's contents have changed, and needs to be reindexed.
   * 
   * @return true if removed from at least one index, false if not found.
   */
  removeDocumentCompletely(doc: D): boolean {
    let removed = false;

    this.index.forEach((docSet: DocumentSet<D>) => {
      if (docSet.delete(doc)) {
        removed = true;
      }
    });

    return removed;
  }

  getDocumentSet(ngram: NGram): DocumentSet<D> {
    return this.lookup<DocumentSet<D>>(
      ngram,
      (docSet: DocumentSet<D>) => { return DocumentSet.from(docSet); /* copy */ },
      () => { return new DocumentSet<D>(); }
    );
  }

  getAllDocuments(): DocumentSet<D> {
    let docs: DocumentSet<D> = new DocumentSet<D>();

    for (let docSet of this.index.values()) {
      docs.addAll(docSet);
    }

    return docs;
  }

  search(phrase: string, ngramLen: number = AppConstants.DEFAULT_NGRAM_LENGTH, substringSearch: boolean = false): DocumentSet<D> {
    //
    // TODO: handle substring searching
    //
    let ngrams: NGram[] = new NGramSplitter(ngramLen).split(phrase);

    if (ngrams.length === 0) {
      return new DocumentSet<D>();
    }

    // intersecting on an empty set always yields empty.
    // thus initialise with at least one ngram.
    let resultingSet: DocumentSet<D> = this.getDocumentSet(ngrams[0]);

    for (let ngram of ngrams) {
      // okay to reuse the first ngram -- equal sets intersection yield the same set.
      resultingSet = resultingSet.intersection(this.getDocumentSet(ngram));
    }
    
    return resultingSet;
  }

  has(ngram: NGram): boolean {
    return this.index.has(ngram);
  }

  clear() {
    this.index.clear();
  }

  private lookup<R>(
    ngram: NGram,
    foundFn: (doc: DocumentSet<D>) => R,
    notFoundFn: () => R
  ): R {
    if (this.has(ngram)) {
      return foundFn(this.index.get(ngram)!);
    } else {
      return notFoundFn();
    }
  }
}
