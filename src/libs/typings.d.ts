declare namespace browser.bookmarks {
  export interface BookmarkTreeNode {
    id: string;
    parentId: string;
    title: string;
    index: string;
    url?: string;
    children?: BookmarkTreeNode[];
    unmodifiable?: string;
  }

  export interface ChangeInfo { 
    title: string;
    url?: string 
  }
  
  export interface MoveInfo {
    parentId: string; 
    index: number; 
    oldParentId: string; 
    oldIndex: number;
  }

  export interface RemoveInfo {
    parentId: string; 
    index: number;
    node: {
      id: string,
      index: number,
      parentId?: string,
      url?: string
    }
  }

  interface CanAddOnChangedListener {
    addListener(fn: (id: string, changeInfo: ChangeInfo) => void): void;
  }

  interface CanAddOnCreatedListener {
    addListener(fn: (id: string, createdNode: BookmarkTreeNode) => void): void;
  }

  interface CanAddOnMovedListener {
    addListener(fn: (id: string, moveInfo: MoveInfo) => void): void;
  }

  interface CanAddOnRemovedListener {
    addListener(fn: (id: string, removeInfo: RemoveInfo) => void): void;
  }

  type Query = string | { query?: string, url?: string, title?: string };

  export var onChanged: CanAddOnChangedListener;
  export var onCreated: CanAddOnCreatedListener;
  export var onMoved: CanAddOnMovedListener;
  export var onRemoved: CanAddOnRemovedListener;

  export function create(obj: { parentId?: string, index?: number, title?: string, url?: string }): Promise<BookmarkTreeNode>;
  export function get(id: string | string[]): Promise<Array<BookmarkTreeNode>>;
  export function getTree(): Promise<Array<BookmarkTreeNode>>;
  export function getSubTree(id: string): Promise<Array<BookmarkTreeNode>>;
  export function search(q: Query): Promise<Array<BookmarkTreeNode>>;
  export function update(id: string, changes: { title?: string, url?: string }): Promise<BookmarkTreeNode>;
  export function remove(id: string): Promise<void>;
}

declare namespace browser.tabs {
  interface CanAddListener {
    addListener(fn: (tabs: browser.tabs.Tab[]) => void): void;
  }

  // https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/tabs/Tab
  export interface Tab {
    title: string;
    url: string;
  }

  export interface TabCreateProperties {
    active?: boolean;
    url?: string;
  }

  export var onUpdated: CanAddListener;
  export var onActivated: CanAddListener;


  export function query(params: object): Promise<browser.tabs.Tab[]>;
  export function create(createProps: TabCreateProperties): Promise<browser.tabs.Tab>;
}

declare namespace browser.windows {
  export var onFocusChanged: browser.tabs.CanAddListener;
}

declare namespace browser.runtime {
  interface CanAddMessageListener {
    addListener(fn: (message: any) => void): void;
  }

  interface CanAddPortListener {
    addListener(fn: (port: Port) => void): void;
  }


  export interface Port {
    name: string;
    error?: { message: string };
    onMessage: CanAddMessageListener;
    onDisconnect: CanAddPortListener;

    postMessage(msg: any): void;
    disconnect(): void;
  }

  export function connect(
    connectInfo?: { name?: string; includeTlsChannelIdOptional?: boolean; },
  ): Port;

  export var onConnect: CanAddPortListener;
}

