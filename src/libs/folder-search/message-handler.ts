import AppConstants from 'app-libs/app-constants';
import { BookmarkFolderDocumentProperties } from 'app-libs/bookmark/bookmark-folder-document';
import { 
  BOOKMARK_FOLDER_INDEXER_SERVICE_PORT,
  FullReindexRequest,
  GetAllBookmarkFoldersRequest, 
  Requests, 
  Responses,
  SearchBookmarkFoldersRequest,
  Status
} from 'app-libs/messages';
import Util from 'app-libs/util';

export interface ResponseReceiver {
  onFolderResults<T extends BookmarkFolderDocumentProperties>(folders: T[]): void;
  onStatus(status: Status, msg?: string): void;
}

export class MessageHandler {
  private port?: browser.runtime.Port;

  constructor(
    private readonly ctx: string,
    private readonly receiver: ResponseReceiver
  ) {
  }

  init(): void {
    this.port = browser.runtime.connect({name: BOOKMARK_FOLDER_INDEXER_SERVICE_PORT});

    this.port.onDisconnect.addListener((p) => {
      Util.logPortDisconnection(Util.CTX_BOOKMARK_DLG, p);
      this.receiver.onStatus(Status.ERROR, Util.getDisconnectedPortMessage(p));

      this.port = undefined;
    });

    this.port.onMessage.addListener((msg: Responses) => {
      Util.log(this.ctx, `Received message on port[${this.port!.name}]`);

      // let receiver handle status for additional logging, etc.
      this.receiver.onStatus(msg.status, msg.errorReason);
      
      // we have defaults
      switch (msg.status) {
        case Status.OK:
          // part of contract and ease of use.
          this.receiver.onFolderResults(msg.documents!);
          return;

        case Status.ERROR:
          Util.log(this.ctx, `Error Response[${msg.errorReason}]`);
          return;

        case Status.INDEX_IN_PROGRESS:
          Util.log(this.ctx, "Indexing in Progress");
          return;

        default:
          Util.assertNever(msg.status);
      }
    });
  }

  searchFor(text: string): void {
    let phrase: string = text.trim();
    Util.log(this.ctx, `Got text[${phrase}]`);

    if (phrase.length < AppConstants.DEFAULT_NGRAM_LENGTH) {
      this.sendMessage(new GetAllBookmarkFoldersRequest());
    } else {
      this.sendMessage(new SearchBookmarkFoldersRequest(phrase));
    }
  }

  requestAllFolders(): void {
    this.sendMessage(new GetAllBookmarkFoldersRequest());    
  }

  issueFullReindexRequest() {
    this.sendMessage(new FullReindexRequest());
  }

  private sendMessage(request: Requests) {
    if (this.port && !this.port.error) {
      this.port.postMessage(request);
    } else {
      let msg: string = "Connection to Indexing Service is broken. Try re-opening the bookmark popup, or restarting the browser.";
      Util.log(this.ctx, msg);
      this.receiver.onStatus(Status.ERROR, msg);
    }
  }
}

