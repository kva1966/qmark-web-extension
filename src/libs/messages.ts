import { BookmarkFolderDocumentProperties } from 'app-libs/bookmark/bookmark-folder-document';
import { DocumentSet } from 'app-libs/indexing/document-set';

export const BOOKMARK_FOLDER_INDEXER_SERVICE_PORT: string = 'bookmark-folder-indexer-search-port';

export enum Status {
  OK,
  INDEX_IN_PROGRESS,
  ERROR
};

export enum RequestType {
  SearchBookmarkFolders,
  GetAllBookmarkFolders,
  FullReindex
}

export interface RequestProperties {
  /**
   * Messages are serialised to JSON, so we can't purely rely on
   * instanceof checks, hence explicitly putting in message types
   * in each request. Client can then cast the typeless JSON object
   * to the typed request interface based on the request type.
   */
  readonly requestType: RequestType;
}

export interface BookmarkFolderDocumentsResponseProperties {
  readonly status: Status;
  /**
   * populated if Status.ERROR
   */
  readonly errorReason?: string;

  /**
   * Empty if no results or error.
   */
  readonly documents: BookmarkFolderDocumentProperties[];
}

export class BookmarkFolderDocumentsResponse implements BookmarkFolderDocumentsResponseProperties {
  private constructor(
    public readonly status: Status, 
    public readonly documents: BookmarkFolderDocumentProperties[] = [],
    public readonly errorReason?: any) {
  }

  static indexInProgress(): BookmarkFolderDocumentsResponse {
    return new BookmarkFolderDocumentsResponse(Status.INDEX_IN_PROGRESS);
  }

  static error(reason: any): BookmarkFolderDocumentsResponse {
    return new BookmarkFolderDocumentsResponse(Status.ERROR, [], reason);
  }

  static ok(documents: BookmarkFolderDocumentProperties[]) {
    return new BookmarkFolderDocumentsResponse(Status.OK, documents);
  }
}

export interface FullReindexRequestProperties extends RequestProperties {  
}

export class FullReindexRequest implements FullReindexRequestProperties {
  readonly requestType: RequestType = RequestType.FullReindex;
}

export interface SearchBookmarkFoldersRequestProperties extends RequestProperties {
  readonly searchPhrase: string;
  readonly substringSearch: boolean;
}

export class SearchBookmarkFoldersRequest implements SearchBookmarkFoldersRequestProperties {
  readonly requestType: RequestType = RequestType.SearchBookmarkFolders;

  constructor(
    public readonly searchPhrase: string, 
    public readonly substringSearch: boolean = false) {
  }
}

export interface GetAllBookmarkFoldersRequestProperties extends RequestProperties {
  readonly sort: boolean;
}

export class GetAllBookmarkFoldersRequest implements GetAllBookmarkFoldersRequestProperties {
  readonly requestType: RequestType = RequestType.GetAllBookmarkFolders;

  constructor(
    public readonly sort: boolean = false) {
  }
}

export type Requests = 
  GetAllBookmarkFoldersRequestProperties |
  SearchBookmarkFoldersRequestProperties |
  FullReindexRequestProperties;

export type Responses =  BookmarkFolderDocumentsResponseProperties; // only one right now.
