import Util from 'app-libs/util';

import AppConstants from 'app-libs/app-constants';
import { NGram, NGramSplitter } from 'app-libs/indexing/ngram';
import { Document, ID } from 'app-libs/indexing/document-set';
import { Bookmarks } from 'app-libs/bookmark/bookmark-util';

const ROOT_DISPLAY_TITLE_PREFIX = "";
const ROOT_SEARCHABLE_TITLE_PREFIX = "";

// http://xahlee.info/comp/unicode_arrows.html
const DISPLAY_TITLE_SEPARATOR = AppConstants.DISPLAY_TITLE_PATH_SEPARATOR;
const SEARCHABLE_TITLE_SEPARATOR = AppConstants.SEARCHABLE_TITLE_PATH_SEPARATOR;
const EMPTY_TITLE_REPLACEMENT = '(no title)';

interface DocumentTitles { 
  displayTitle: string; 
  searchableTitle: string;
}

export interface BookmarkFolderDocumentProperties extends Document {
  readonly searchableTitle: string;
  readonly displayTitle: string;
  readonly ngrams: ReadonlyArray<NGram>;
}

export class BookmarkFolderDocument implements BookmarkFolderDocumentProperties {
  readonly id: ID;
  readonly searchableTitle: string;
  readonly displayTitle: string;
  readonly ngrams: ReadonlyArray<NGram>;

  constructor(id: ID, searchableTitle: string, displayTitle: string, ngramN: number) {
    this.id = id;
    this.searchableTitle = searchableTitle;
    this.displayTitle = displayTitle;
    this.ngrams = new NGramSplitter(ngramN).split(searchableTitle);
  }

  /**
   * To search only.
   */
  static toSearchBy(id: ID): BookmarkFolderDocument {
    return new BookmarkFolderDocument(id, '', '', 1000);
  }

  static isFolder(id: ID): Promise<boolean> {
    return browser.bookmarks.get(id).then(nodes => {
      return BookmarkFolderDocument.isFolderNode(nodes[0]);
    })
  };

  static isFolderNode(node: browser.bookmarks.BookmarkTreeNode): boolean {
    return !node.url;
  }

  static isFolderNode2(node: { url?: string }): boolean {
    return !node.url;
  }

  toString(): string {
    return `${this.id}|${this.displayTitle}|${this.searchableTitle}|`;
  }
}

export class BookmarkFolderDocuments {
  static buildFolderDocumentsFromRoot(ctx: string, ngramLen: number): Promise<BookmarkFolderDocument[]> {
    let results: BookmarkFolderDocument[] = [];

    return browser
      .bookmarks
      .getTree()
      .then((rootTreeArr) => { return BookmarkFolderDocuments.toDocuments(ctx, rootTreeArr[0], results, ngramLen) })
      .catch((reason) => { return Promise.reject(`Failed to create folder docs[${reason}]`) });
  }

  /**
   * @return documents for the supplied folder ID and its children recursively.
   */
  static buildFolderDocuments(ctx: string, folderId: string, ngramLen: number): Promise<BookmarkFolderDocument[]> {
    let results: BookmarkFolderDocument[] = [];

    return BookmarkFolderDocuments
      .newFolderDocument(ctx, folderId, ngramLen)
      .then((doc: BookmarkFolderDocument) => {
        // folder
        results.push(doc);

        // children
        return browser.bookmarks.getSubTree(doc.id).then(nodes => {
          let folderNode = nodes[0];

          if (folderNode.children) {
            folderNode.children.forEach(child => {
              BookmarkFolderDocuments.toDocuments(ctx, child, results, ngramLen, false, doc.searchableTitle, doc.displayTitle);
            })
          }

          return results;
        });
      })
  }


  /**
   * Create bookmark in all supplied folders. A bookmark tree node created per parent folder.
   */
  static create(
    url: string, title: string, parentFolders: BookmarkFolderDocument[]
  ): Promise<browser.bookmarks.BookmarkTreeNode[] | any[]> {
    let promises: Promise<browser.bookmarks.BookmarkTreeNode>[] = []

    for (let folder of parentFolders) {
      promises.push(
        browser.bookmarks.create({
          parentId: folder.id,
          title: title,
          url: url
        })
      );
    }

    return Promise.all(promises);
  }


  static logDocs(ctx: string, bookmarkFolders: ReadonlyArray<BookmarkFolderDocument>) {
    bookmarkFolders.forEach((d) => { Util.log(ctx, d); });
  }

  static newFolderDocument(ctx: string, bookmarkFolderId: ID, ngramLen: number): Promise<BookmarkFolderDocument> {
    return Bookmarks
      .resolveBookmarkHierarchy(ctx, bookmarkFolderId)
      .then((hierarchy: browser.bookmarks.BookmarkTreeNode[]) => {
        let titles: string[] = hierarchy.map((node) => { return node.title; });

        // convoluted, but keeps title creation logic consistent between initial indexing and incremental index changes.
        let titleResults: DocumentTitles = { displayTitle: ROOT_DISPLAY_TITLE_PREFIX, searchableTitle: ROOT_SEARCHABLE_TITLE_PREFIX };

        titles.forEach((t: string, index: number) => {
          titleResults = BookmarkFolderDocuments.buildDocumentTitle(t, titleResults.searchableTitle, titleResults.displayTitle, index === 0);
        })

        let folder = new BookmarkFolderDocument(bookmarkFolderId, titleResults.searchableTitle, titleResults.displayTitle, ngramLen);
        BookmarkFolderDocuments.logCreatingNewFolder(ctx, folder);

        return folder;
      });
  }

  private static buildDocumentTitle(
    baseTitle: string, searchableTitlePrefix: string, displayTitlePrefix: string, isRoot: boolean): DocumentTitles {
    let title: string = Util.isEmpty(baseTitle) && !isRoot ? EMPTY_TITLE_REPLACEMENT : baseTitle.trim();

    return {
      displayTitle: `${displayTitlePrefix}${title}${DISPLAY_TITLE_SEPARATOR}`,
      searchableTitle: `${searchableTitlePrefix}${title}${SEARCHABLE_TITLE_SEPARATOR}`
    };
  }

  private static toDocuments(
    ctx: string,
    tree: browser.bookmarks.BookmarkTreeNode,
    results: BookmarkFolderDocument[],
    ngramLen: number,
    isRootTree: boolean = true,
    rootSearchableTitlePrefix: string = ROOT_SEARCHABLE_TITLE_PREFIX,
    rootDisplayTitlePrefix: string = ROOT_DISPLAY_TITLE_PREFIX
  ): BookmarkFolderDocument[] {
    let rootTree = isRootTree;

    let _toDocuments = (searchableTitle: string, displayTitle: string, tree: browser.bookmarks.BookmarkTreeNode) => {
      let titles = BookmarkFolderDocuments.buildDocumentTitle(tree.title, searchableTitle, displayTitle, rootTree);

      if (rootTree) {
        rootTree = false;
      }

      if (BookmarkFolderDocument.isFolderNode(tree)) {
        let folder = new BookmarkFolderDocument(tree.id, titles.searchableTitle, titles.displayTitle, ngramLen);
        BookmarkFolderDocuments.logCreatingNewFolder(ctx, folder);
        results.push(folder);
      }

      if (!tree.children) {
        return results;
      }

      for (let childTree of tree.children) {
        _toDocuments(titles.searchableTitle, titles.displayTitle, childTree);
      }

      return results;
    };

    _toDocuments(rootSearchableTitlePrefix, rootDisplayTitlePrefix, tree);

    return results;
  }

  private static logCreatingNewFolder(ctx: string, folder: BookmarkFolderDocument) {
    Util.log(ctx, `[${ctx}] Creating BookmarkFolderDocument -> ${folder.id}|${folder.searchableTitle}`);
  }

}
