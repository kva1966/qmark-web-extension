import AppConstants from 'app-libs/app-constants';
import Util from 'app-libs/util';
import { ID } from 'app-libs/indexing/document-set';
import { BookmarkFolderDocument } from 'app-libs/bookmark/bookmark-folder-document';

export class Folder {
  constructor(readonly id: ID, readonly title: string) {}
}

export class BookmarkPath {
  readonly id: ID;
  readonly url: string;
  readonly title: string;
  readonly parentFolders: Folder[]

  static from(resolvedPath: browser.bookmarks.BookmarkTreeNode[]): BookmarkPath {
    return new BookmarkPath(resolvedPath);
  }

  private constructor(resolvedPath: browser.bookmarks.BookmarkTreeNode[]) {
    // Leaf Details
    let leaf: browser.bookmarks.BookmarkTreeNode = resolvedPath[resolvedPath.length - 1];
    [this.id, this.url, this.title] = [leaf.id, leaf.url === undefined ? '' : leaf.url, leaf.title];

    // Folders
    this.parentFolders = []
    for (let i = 0; i < resolvedPath.length - 1; i++) {
      this.parentFolders.push(new Folder(resolvedPath[i].id, resolvedPath[i].title))
    }
  }
}

export class Bookmarks {
  /**
   * @param ctx logging/error context.
   * @param bookmarkId id to resolve parent hierch.
   * @return Promise([full, path, to, bookmark]) - parent path nodes appear 
   *   before children in the array. Last element is the node for bookmarkId.
   */
  static resolveBookmarkHierarchy(ctx: string, bookmarkId: ID): Promise<browser.bookmarks.BookmarkTreeNode[]> {
    let results: browser.bookmarks.BookmarkTreeNode[] = [];

    let resolveHierarchy: (id: ID) => Promise<browser.bookmarks.BookmarkTreeNode[]> =
      (id) => {
        let promise: Promise<browser.bookmarks.BookmarkTreeNode[]> = browser.bookmarks.get(id);

        return promise.then((nodes): Promise<browser.bookmarks.BookmarkTreeNode[]> => {
          let node = nodes[0];
          results.push(node);

          // this is NOT recursion!
          if (node.parentId) {
            return resolveHierarchy(node.parentId);
          } else {
            return Promise.resolve(results.reverse());
          }
        }).catch((reason) => {
          Util.log(ctx, `Failed to resolve bookmark id[${id}], reason[${reason}]`, reason);
          throw new Error(reason);
        })
      };

    // Resolve, returning a new promise, so the original promise's then() call hierarchy
    // is not disturbed.
    return Promise.resolve(resolveHierarchy(bookmarkId));
  }

  static search(ctx: string, query: string, includeFolders: boolean = false): Promise<Array<BookmarkPath>> {
    return browser.bookmarks.search(query)
      // for each node in result, resolve full path
      .then((results: browser.bookmarks.BookmarkTreeNode[]) => {
        let promises: Promise<browser.bookmarks.BookmarkTreeNode[]>[] = [];

        for (let result of results) {
          if (!includeFolders && BookmarkFolderDocument.isFolderNode(result)) {
            continue;
          }
          
          promises.push(Bookmarks.resolveBookmarkHierarchy(ctx, result.id));
        }

        return Promise.all(promises);
      })
      // then build simpler BookmarkPath objects
      .then((resolvedBookmarks: browser.bookmarks.BookmarkTreeNode[][]) => {
        return Promise.resolve(
          resolvedBookmarks.map((resolvedBookmark): BookmarkPath => { 
            return BookmarkPath.from(resolvedBookmark); 
          })
        );
      })
      .catch((reason) => {
        Util.log(ctx, `Failed to search for bookmarks matching[${query}], reason[${reason}]`, reason);
        throw new Error(reason);
      });
  }
}