import Util from 'app-libs/util';
import { InvertedIndex } from 'app-libs/indexing/inverted-index';
import { BookmarkFolderDocument, BookmarkFolderDocuments } from 'app-libs/bookmark/bookmark-folder-document';
import AppConstants from 'app-libs/app-constants';

interface Error {
  reason: string;
}

export default class BookmarkFolderIndexer {
  static readonly IndexInProgressMessage = 'Indexing in Progress, could not complete requested index operation.';
  private static readonly NGRAM_LEN = AppConstants.DEFAULT_NGRAM_LENGTH;
  private readonly index: InvertedIndex<BookmarkFolderDocument> = new InvertedIndex<BookmarkFolderDocument>();
  private indexingInProgress: boolean = false;
  private error?: Error = undefined;

  initIndex(): Promise<BookmarkFolderIndexer> {
    this.prepareForFullReindex();

    return this.requestIndexOperation("Initial Index Building", indexer => {
      return BookmarkFolderDocuments.buildFolderDocumentsFromRoot(Util.CTX_FOLDER_INDEXER, BookmarkFolderIndexer.NGRAM_LEN)
        .then((docs: BookmarkFolderDocument[]): Promise<BookmarkFolderIndexer> => {
          Util.logFolderIndexer("Created Documents");
          BookmarkFolderDocuments.logDocs(Util.CTX_FOLDER_INDEXER, docs);

          Util.logFolderIndexer("Building Index from Documents");

          docs.forEach((doc: BookmarkFolderDocument) => {
            indexer.addFolder(doc);
          });

          Util.logFolderIndexer("Index Built");
          return Promise.resolve(indexer);
        }).catch(reason => { throw new Error(reason); });
    })
  }

  isIndexing(): boolean {
    return this.indexingInProgress;
  }

  getAll(): Promise<BookmarkFolderDocument[]> {
    Util.assertTrue(this.isIndexing() === false, "Indexing in progress");
    return this.requestIndexOperation(`Getting All Documents`, indexer => {
      return Promise.resolve(
        indexer.index
          .getAllDocuments()
          .getAll()
      );
    });
  }

  search(phrase: string, substringSearch: boolean = false): Promise<BookmarkFolderDocument[]> {
    return this.requestIndexOperation(`Searching for[${phrase}]`, indexer => {
      return Promise.resolve(
        indexer.index
          .search(phrase, BookmarkFolderIndexer.NGRAM_LEN)
          .getAll()
      );
    });
  }

  getError(): Error | undefined {
    return this.error;
  }

  changeHandler(bookmarkId: string, info: browser.bookmarks.ChangeInfo) {
    Util.logFolderIndexer(`change event, bookmark id[${bookmarkId}]`, info);
    this.reindexFolderAndChildren(`Bookmark Change[${bookmarkId}]`, bookmarkId);
  }

  createHandler(bookmarkId: string, node: browser.bookmarks.BookmarkTreeNode) {
    Util.logFolderIndexer(`create event[${bookmarkId}]`, node);

    if (BookmarkFolderDocument.isFolderNode2(node)) {
      this.requestIndexOperation(`Bookmark Created[${bookmarkId}]`, indexer => {
        return BookmarkFolderDocuments.newFolderDocument(Util.CTX_FOLDER_INDEXER, bookmarkId, BookmarkFolderIndexer.NGRAM_LEN)
          .then(folder => { indexer.addFolder(folder); })
          .catch(reason => { throw new Error(reason); });
      });
    }
  }

  moveHandler(bookmarkId: string, info: browser.bookmarks.MoveInfo) {
    Util.logFolderIndexer(`move event[${bookmarkId}]`, info);
    this.reindexFolderAndChildren(`Bookmark Moved[${bookmarkId}]`, bookmarkId);
  }

  removedHandler(bookmarkId: string, info: browser.bookmarks.RemoveInfo) {
    /*
     * Folder will cause a remove message per item - the folder and all its
     * children recursively each get a message. So, we can just delete the 
     * single document entry per invocation of this handler.
     *
     * The only potential issue is if as we are indexing, we reject incoming
     * messages -- not quite sure how async listeners affect in progress
     * promises.
     */
    Util.logFolderIndexer(`removing event[${bookmarkId}]`, info);

    // We can't do a full-folder resolve here -- since the bookmark will not
    // be found. We rely on the info object instead -- has a URL if non-folder
    // no URL otherwise.
    if (BookmarkFolderDocument.isFolderNode2(info.node)) {
      this.requestIndexOperation(`Bookmark Remove[${bookmarkId}]`, indexer => {
        indexer.removeFolder(bookmarkId);
        return Promise.resolve(indexer);
      });
    }
  }

  private prepareForFullReindex() {
    // not very DRY, but using direct access to bypass any guards.
    this.error = undefined;
    this.indexingInProgress = false;
    this.index.clear();
  }

  private reindexFolderAndChildren(opInfo: string, bookmarkId: string) {
    BookmarkFolderDocument.isFolder(bookmarkId).then(isFolder => {
      if (!isFolder) {
        return;
      }

      this.requestIndexOperation(opInfo, indexer => {
        return BookmarkFolderDocuments.buildFolderDocuments(Util.CTX_FOLDER_INDEXER, bookmarkId, BookmarkFolderIndexer.NGRAM_LEN)
          .then(folders => {
            folders.forEach((f) => { indexer.reindexFolder(f) });
            return indexer;
          })
          .catch(reason => { throw new Error(reason); });
      });
    })
  }

  private requestIndexOperation<T>(opInfo: string, operation: (indexer: BookmarkFolderIndexer) => Promise<T>): Promise<T> {
    if (this.error) {
      return Promise.reject(`Error[${this.error.reason}]`)
    }

    if (this.isIndexing()) {
      return Promise.reject(BookmarkFolderIndexer.IndexInProgressMessage);
    }

    /*
     * Index Operation
     * -> apply in progress
     * -> make changes
     * -> then either progress done
     * -> or error returned
     */
    Util.logFolderIndexer(opInfo);
    this.markIndexOperationInProgress(opInfo);

    return operation(this).then(ignored => {
      this.markIndexOperationCompleted(opInfo);
      return ignored;
    }).catch(reason => {
      Util.logFolderIndexer(`Caught Error during Index Operation[${reason}]`);
      this.markIndexOperationError(opInfo, reason);
      throw new Error(reason);
    });
  }

  private reindexFolder(doc: BookmarkFolderDocument) {
    this.removeFolder(doc.id);
    this.addFolder(doc);
  }

  private addFolder(doc: BookmarkFolderDocument) {
    this.assertInIndexUpdateRequest();

    for (let ngram of doc.ngrams) {
      this.index.addDocument(ngram, doc);
    }
  }

  private removeFolder(bookmarkId: string) {
    this.assertInIndexUpdateRequest();

    this.index.removeDocumentCompletely(BookmarkFolderDocument.toSearchBy(bookmarkId));
  }

  private markIndexOperationInProgress(op: string) {
    Util.logFolderIndexer(`[Begin] Index Operation - ${op}`);
    this.indexingInProgress = true;
  }

  private markIndexOperationCompleted(op: string) {
    this.assertInIndexUpdateRequest();

    Util.logFolderIndexer(`[Completed] Index Operation - ${op}`);
    this.indexingInProgress = false;
  }

  private markIndexOperationError(op: string, reason: any) {
    this.assertInIndexUpdateRequest();

    let msg = `[ERROR] Index Operation: ${op} Error[${reason}] - indexer service is unusable! 
      Report to dev with logs from the browser, console (in Firefox, this is distinct from the 
      Web Developer console). Errors are likely as a result of the QMark's index going out of
      synch with the browser's bookmark store. First try reindexing, failing that, try 
      reloading the extension.`;
    this.error = { reason: msg };
    Util.logFolderIndexer(msg, reason);
  }

  private assertInIndexUpdateRequest() {
    Util.assertTrue(this.indexingInProgress === true, "Unexpected state, be sure to call from requestIndexUpdate()");
  }
}
