export default class AppConstants {
  static readonly DEFAULT_NGRAM_LENGTH = 3;

  // http://xahlee.info/comp/unicode_arrows.html
  static readonly DISPLAY_TITLE_PATH_SEPARATOR = ' ➢ ';

  static readonly SEARCHABLE_TITLE_PATH_SEPARATOR = '/';

  static readonly MAX_DISPLAYABLE_URL_LENGTH = 66;

  static readonly URL_OPEN_MARKER = '«';
  static readonly URL_CLOSE_MARKER = '»';
  static readonly URL_INCOMPLETE_SUFFIX = '…';

  static readonly SEARCH_INPUT_DEBOUNCE_RATE_MS = 260;
}

