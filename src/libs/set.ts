/**
 * A better Set allowing for equality and common set operations.
 */
export default class Sett<T> extends Set<T> {
  private set: Set<T>;

  static of<T>(valSet: Sett<T>): Sett<T> {
    return new Sett<T>(Array.from(valSet));
  }

  constructor(values?: T[]) {
    super(values);
  }

  isSupersetOf(other: Sett<T>): boolean {
    for (let elem of other) {
      if (!this.has(elem)) {
        return false;
      }
    }

    return true;
  }

  map<R>(fn: (elem: T) => R): R[] {
    let results: R[] = [];

    this.forEach(e => { 
      results.push(fn(e)); 
    });

    return results;
  }

  union(other: Sett<T>): Sett<T> {
    let union = Sett.of<T>(this);

    for (let elem of other) {
      union.add(elem);
    }

    return union;
  }

  intersection(other: Sett<T>): Sett<T> {
    let intersection = new Sett<T>();

    other.forEach((e) => {
      if (this.has(e)) {
        intersection.add(e);
      }
    })

    return intersection;
  }

  difference(other: Sett<T>): Sett<T> {
    let difference = Sett.of<T>(this);

    for (let elem of other) {
      difference.delete(elem);
    }

    return difference;
  }

  minus(other: Sett<T>): Sett<T> {
    return this.difference(other);
  }

  isEmpty(): boolean {
    return this.size === 0;
  }

  equals(other: Sett<T>): boolean {
    // equal if same size and no difference!
    return other.size == this.size && this.difference(other).isEmpty();
  }
}
