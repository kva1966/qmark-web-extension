/*
 * Manifest
 */
require('./manifest.json')

/*
 * Images, Styles, Pages
 */

 // JQuery UI
require('jquery-ui-dist/jquery-ui.css');
require.context('jquery-ui-dist/images/', true);

// App Pages and assets
require.context('./ui', true);
require.context('./assets', true);
