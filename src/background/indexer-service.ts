import BookmarkFolderIndexer from 'app-libs/bookmark/bookmark-folder-indexer';
import {
  BookmarkFolderDocumentsResponse,
  GetAllBookmarkFoldersRequestProperties,
  Requests,
  RequestType,
  Responses,
  SearchBookmarkFoldersRequestProperties,
} from 'app-libs/messages';
import Util from 'app-libs/util';

Util.logIndexerService("Loading Bookmark Indexing Script");


/**
 * Background service handling messages to manipulate the indexer.
 */
export default class IndexerService {
  private port?: browser.runtime.Port;
  private readonly indexer: BookmarkFolderIndexer;

  constructor() {
    this.indexer = new BookmarkFolderIndexer();
  }

  start() {
    this
      .indexer
      .initIndex()
      .then((indexer) => { this.listenForIndexChangeEvents(); });

    this.listenForConnection();
  }

  private listenForIndexChangeEvents() {
    // wrapped to get proper 'this' reference.
    browser.bookmarks.onChanged.addListener((id, info) => { this.indexer.changeHandler(id, info); });
    browser.bookmarks.onCreated.addListener((id, node) => { this.indexer.createHandler(id, node); });
    browser.bookmarks.onMoved.addListener((id, info) => { this.indexer.moveHandler(id, info) });
    browser.bookmarks.onRemoved.addListener((id, info) => { this.indexer.removedHandler(id, info); });
  }

  private listenForConnection() {
    /*
     * Background script is a listener. It receives connections only.
     */
    // wrap in function to get proper 'this' reference
    browser.runtime.onConnect.addListener((port: browser.runtime.Port) => {
      Util.logIndexerService(`Received Connection -> Connected to[${port.name}]`);

      this.port = port;

      // wrap in function to get proper 'this' reference within MSG
      this.port.onMessage.addListener((msg: any) => {
        this.onMessageReceived(msg);
      });

      this.port.onDisconnect.addListener((p) => {
        Util.logPortDisconnection(Util.CTX_INDEXER_SERVICE, p);
        this.port = undefined;
      });
    });
  }

  private sendMessage(msg: Responses): void {
    if (this.port && !this.port.error) {
      this.port.postMessage(msg);
    } else {
      Util.logIndexerService('Port is disconnected or unitialised. Unable to send message.');
    }
  }

  private onMessageReceived(msg: Requests) {
    Util.logIndexerService(`Received message ${msg}`, msg);

    if (!msg) {
      return;
    }

    let determineRejectionResponse = (reason: any): BookmarkFolderDocumentsResponse => {
      return reason === BookmarkFolderIndexer.IndexInProgressMessage ?
        BookmarkFolderDocumentsResponse.indexInProgress() : BookmarkFolderDocumentsResponse.error(reason);
    }

    let requestType: RequestType = msg.requestType;

    switch (requestType) {
      case RequestType.FullReindex:
        Util.logIndexerService('Full Reindex Requested. Will decimate current index and rebuild.');
        this.indexer
          .initIndex()
          .catch(reason => { this.sendMessage(BookmarkFolderDocumentsResponse.error(reason)) });
        // no special success response, never in progress, will always be attempted.
        return;

      case RequestType.GetAllBookmarkFolders:
        Util.logIndexerService("All Folders Requested");
        this.indexer
          .getAll()
          .then(docs => { this.sendMessage(BookmarkFolderDocumentsResponse.ok(docs)); })
          .catch(reason => { this.sendMessage(determineRejectionResponse(reason)); });
        return;

      case RequestType.SearchBookmarkFolders:
        let req: SearchBookmarkFoldersRequestProperties = msg as SearchBookmarkFoldersRequestProperties;
        Util.logIndexerService(`search[${req.searchPhrase}], substringSearch[${req.substringSearch}]`);
        this.indexer
          .search(req.searchPhrase, req.substringSearch)
          .then(docs => { this.sendMessage(BookmarkFolderDocumentsResponse.ok(docs)); })
          .catch(reason => { this.sendMessage(determineRejectionResponse(reason)); })
        return;

      default:
        Util.log(`Unknown request type at runtime[${msg.requestType}]`, msg);
        Util.assertNever(requestType);
    }
  }
}
