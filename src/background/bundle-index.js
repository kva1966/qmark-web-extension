// Background scripts -- app libs will be recursively included from imports
require.context('.', true);

// Execute background script(s) by importing module.
import IndexerService from 'background/indexer-service';

new IndexerService().start();

