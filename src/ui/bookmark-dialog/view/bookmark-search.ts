import * as jQuery from 'jquery';

import AppConstants from 'app-libs/app-constants';
import { Bookmark, BookmarkEntry, ID, Field } from 'ui/bookmark-dialog/model';
import ViewCommons from 'ui/bookmark-dialog/view/common';
import BookmarkEditCommons from 'ui/bookmark-dialog/view/bookmark-edit-common';
import { BookmarkPath } from 'app-libs/bookmark/bookmark-util';
import Util from 'app-libs/util';

const jq = jQuery;

const availableBookmarksDivIdSelector = '#bs-available-bookmarks';
const bookmarkUrlInputIdSelector = '#bs-bookmark-url';
const bookmarkTitleInputIdSelector = '#bs-bookmark-title';
const bookmarkSearchKeyInputIdSelector = '#bs-search-key';
const bookmarkEntryClass = 'bookmark-entry';
const bookmarkEntryClassSelector = `.${bookmarkEntryClass}`;
const bookmarkIdAttr = 'data-bookmark-id';
const modifyBookmarkDialogSelector = '#bs-modify-bookmark-dialog';
const modifyBookmarkDialogWidth = 680;
const modifyBookmarkDialogHeight = 222;

type BookmarkEntryClickedFn = (bookmark: BookmarkEntry) => void;

const searchHint = `Hint: Type ${AppConstants.DEFAULT_NGRAM_LENGTH} characters or more to search in bookmark titles and URLs. Folder text is not searched here.`;

export default class BookmarkSearchView {
  private readonly bookmarkEditCommons: BookmarkEditCommons = 
    new BookmarkEditCommons(bookmarkTitleInputIdSelector, bookmarkUrlInputIdSelector);
  private readonly modifyBookmarkDialog: any;
  private readonly availableBookmarksMap: Map<ID, BookmarkEntry> = new Map<ID, BookmarkEntry>();
  private bookmarkEntryClickedHandler?: BookmarkEntryClickedFn;
  private bookmarkEntryMiddleClickedHandler?: BookmarkEntryClickedFn;
  private bookmarkEntryShiftClickedHandler?: BookmarkEntryClickedFn;

  constructor() {
    /*
     * Modify Bookmark Dialog
     */
    this.modifyBookmarkDialog = $(modifyBookmarkDialogSelector).dialog({
      autoOpen: false,
      width: modifyBookmarkDialogWidth,
      height: modifyBookmarkDialogHeight,
      modal: true
    });

    this.attachEventHandlers();
  }

  onBookmarkSearchText(fn: (s: string) => void) {
    ViewCommons.keyUpHandler(bookmarkSearchKeyInputIdSelector, fn);
  }

  onBookmarkEntryClicked(fn?: BookmarkEntryClickedFn) {
    this.bookmarkEntryClickedHandler = fn;
  }

  onBookmarkEntryMiddleClicked(fn?: BookmarkEntryClickedFn) {
    this.bookmarkEntryMiddleClickedHandler = fn;
  }

  onBookmarkEntryShiftClicked(fn?: BookmarkEntryClickedFn) {
    this.bookmarkEntryShiftClickedHandler = fn;
  }

  setAvailableBookmarks(bookmarkEntries: BookmarkEntry[]) {
    this.availableBookmarksMap.clear();

    for (let entry of bookmarkEntries) {
      this.availableBookmarksMap.set(entry.id, entry);
    }

    this.setEntriesInUi(bookmarkEntries);
  }

  showError(
    errorMsg: string = "Unknown Error. Argh. Check the browser toolbox logs and let the stupid dev know."
  ) {
    ViewCommons.showStatusDialog("Error", errorMsg);
  }

  markBookmarkEditDialogError(field: Field) {
    this.bookmarkEditCommons.toggleError(field, true);
  }

  openModifyBookmarkDialog(bookmarkEntry: BookmarkEntry,
      updateFn: (bookmark: Bookmark) => void,
      deleteFn: (bookmark: Bookmark) => void) {
    let _view = this;
    this.bookmarkEditCommons.setBookmark(bookmarkEntry.toNewBookmark());

    this.modifyBookmarkDialog
      .dialog('option', 'buttons', [
        {
          text: 'Delete',
          icon: 'ui-icon-trash',
          class: 'danger-highlight',
          click: function() {
            deleteFn(_view.getBookmarkFromDom(bookmarkEntry.id));
          }
        },

        {
          text: 'Update',
          icon: 'ui-icon-pencil',
          click: function() {
            updateFn(_view.getBookmarkFromDom(bookmarkEntry.id));
          }
        }
      ])
      .dialog('open');
  }

  closeModifyBookmarkDialog() {
    this.modifyBookmarkDialog.dialog('close');
  }

  private getBookmarkFromDom(id: string | undefined): Bookmark {
    return this.bookmarkEditCommons.getBookmark([], id);
  }

  private setEntriesInUi(entries: BookmarkEntry[]): void {
    let toEntryElement = (entry: BookmarkEntry): string => {
      return `<div class="${bookmarkEntryClass}" ${bookmarkIdAttr}="${entry.id}">${entry.displayTitle}</div><br />`;
    };

    let container = jq(availableBookmarksDivIdSelector);

    container.empty();

    entries.forEach((entry: BookmarkEntry) => {
      container.append(toEntryElement(entry));
    })
  }

  private attachEventHandlers() {
    let view = this;

    /*
     * Bookmark Selection Event Handlers - using Delegated Event Handling:
     *
     * Event handler on parent element, but then selecting on child elements.
     * This allows us to dynamically modify child elements without resetting 
     * the event handling on them each time.
     */
    jq(availableBookmarksDivIdSelector).on('mousedown', bookmarkEntryClassSelector, function(event: any /* complex jquery types! */) {
      let jqElem: any = jq(this);
      let id: ID = jqElem.attr(bookmarkIdAttr) as ID;

      let handlerFn: BookmarkEntryClickedFn | undefined;

      switch (event.which) {
        case 1:
          handlerFn = event.shiftKey ? view.bookmarkEntryShiftClickedHandler : view.bookmarkEntryClickedHandler;
          break;

        case 2:
          handlerFn = view.bookmarkEntryMiddleClickedHandler;
          break;

        default:
          break; // ignore
      }

      if (handlerFn !== undefined) {
        ViewCommons.animateSelection(jqElem);
        handlerFn(view.availableBookmarksMap.get(id)!);
      }
    });

    /*
     * Folder Search Input
     */
    ViewCommons.inputElementHintingHandler(bookmarkSearchKeyInputIdSelector, searchHint);

     /*
      * Edit Dialog
      */
    this.bookmarkEditCommons.attachErrorEventHandling();
  }
}

