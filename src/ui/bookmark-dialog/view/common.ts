import * as jQuery from 'jquery';
const jq = jQuery;

export default class ViewCommons {
  static showStatusDialog(title: string, msg: string) {
    let html: string = `
<div id="status-dialog" title="${title}">
  <p>${msg}</p>
</div>`;

    jq(html).dialog({
      modal: true,
      buttons: {
        OK: function() {
          jq(this).dialog("close");
        }
      }
    });
  }

  static keyUpHandler(inputElemSelector: string, fn: (s: string) => void) {
    let input = jq(inputElemSelector);
    // using keydown or keypress misses the last character!
    input.keyup(() => {
      let s: any = input.val();
      if (!s) {
        fn('');
      } else {
        fn(s);
      }
    });
  }

  static inputElementHintingHandler(inputElemSelector: string, hintText: string) {
    let input = jq(inputElemSelector);
    let searchHinting = (showHint: boolean) => {
     if (showHint) {
       input.attr('placeholder', hintText);
     } else {
       input.removeAttr('placeholder');
     }
    }

    // initial state.
    searchHinting(true);

    input.focusout(() => {
     searchHinting(!input.val());
    });

    input.keyup(() => {
     searchHinting(!input.val());
    });
  }

  static animateSelection(jqueryElem: any) {
    let durationMs: number = 40;

    jqueryElem
      .fadeIn(durationMs)
      .fadeOut(durationMs)
      .fadeIn(durationMs)
      .fadeOut(durationMs)
      .fadeIn(durationMs);
  }

}

