import { BookmarkFolderDocument } from 'app-libs/bookmark/bookmark-folder-document';
import { Bookmark, Field, ID } from 'ui/bookmark-dialog/model';

const textInputClass = 'text-input';
const textInputErrorClass = 'text-input-error';

import Util from 'app-libs/util';

import * as jQuery from 'jquery';
const jq = jQuery;

export default class BookmarkEditCommons {
  constructor(
    private readonly titleInputIdSelector: string,
    private readonly urlInputIdSelector: string
  ) {}


  toggleError(field: Field, error: boolean) {
    let swapClass = (elemSelector: string, classIn: string, classOut: string, reverse: boolean = false) => {
      let e = jq(elemSelector);

      if (reverse) {
        e.removeClass(classIn);
        e.addClass(classOut);
      } else {
        e.removeClass(classOut);
        e.addClass(classIn);
      }
    }

    switch (field) {
      case Field.BookmarkTitle:
        swapClass(this.titleInputIdSelector, textInputErrorClass, textInputClass, !error);
        return;

      case Field.BookmarkUrl:
        swapClass(this.urlInputIdSelector, textInputErrorClass, textInputClass, !error);
        return;

      default:
        return;
    }
  }

  setBookmark(bookmark: Bookmark): void {
    jq(this.urlInputIdSelector).val(bookmark.url);
    jq(this.titleInputIdSelector).val(bookmark.title);
  }

  getBookmark(selectedFolders: BookmarkFolderDocument[] = [], id: string | undefined = undefined): Bookmark {
    let url: string = Util.trimmedOrDefaultIfEmpty(jq(this.urlInputIdSelector).val() as string, '');
    let title: string = Util.trimmedOrDefaultIfEmpty(jq(this.titleInputIdSelector).val() as string, '');
    return new Bookmark(url, title, selectedFolders, id);
  }

  attachErrorEventHandling() {
    [
      { selector: this.titleInputIdSelector, field: Field.BookmarkTitle },
      { selector: this.urlInputIdSelector, field: Field.BookmarkUrl }
    ].forEach((obj) => {
      let o = jq(obj.selector);

      o.focusin(() => {
        this.toggleError(obj.field, false);
      });

      o.focusout(() => {
        if (!this.getBookmark().validateField(obj.field)) {
          this.toggleError(obj.field, true);
        }
      });
    });
  }
}