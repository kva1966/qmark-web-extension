import * as jQuery from 'jquery';

import { BookmarkFolderDocumentProperties } from 'app-libs/bookmark/bookmark-folder-document';
import { ID } from 'app-libs/indexing/document-set';
import { Status } from 'app-libs/messages';
import Util from 'app-libs/util';
import AppConstants from 'app-libs/app-constants';

import { Bookmark, Field } from 'ui/bookmark-dialog/model';
import ViewCommons from 'ui/bookmark-dialog/view/common';
import BookmarkEditCommons from 'ui/bookmark-dialog/view/bookmark-edit-common';

const jq = jQuery;

const availableFoldersDivIdSelector = '#btf-available-folders';
const bookmarkUrlInputIdSelector = '#btf-bookmark-url';
const bookmarkTitleInputIdSelector = '#btf-bookmark-title';
const bookmarkButtonIdSelector = '#btf-bookmark-button';
const folderIdAttr = 'data-folder-id';
const folderSearchKeyInputIdSelector = '#btf-folder-search-key';
const folderEntryClass = 'folder-entry';
const folderEntryClassSelector = `.${folderEntryClass}`;
const folderEntryRemovableClass = 'removable';
const folderEntryRemovableClassSelector = `.${folderEntryRemovableClass}`;
const noSelectedFoldersErrorClass = 'no-selected-folders-error';
const reindexButtonIdSelector = '#btf-reindex-button';
const selectedFoldersDivIdSelector = '#btf-selected-folders';
const textInputErrorClass = 'text-input-error';
const textInputClass = 'text-input';

const searchHint = `Hint: Type ${AppConstants.DEFAULT_NGRAM_LENGTH} or more characters to begin searching. Use / to separate paths.`;

export default class BookmarkToFoldersView {
  private readonly bookmarkEditCommons: BookmarkEditCommons = 
    new BookmarkEditCommons(bookmarkTitleInputIdSelector, bookmarkUrlInputIdSelector);
  private readonly availableFoldersMap: Map<ID, BookmarkFolderDocumentProperties> = 
    new Map<ID, BookmarkFolderDocumentProperties>();
  private readonly selectedFoldersMap: Map<ID, BookmarkFolderDocumentProperties> = 
    new Map<ID, BookmarkFolderDocumentProperties>();

  constructor() {
    this.attachEventHandlers();
  }

  setBookmark(bookmark: Bookmark): void {
    this.bookmarkEditCommons.setBookmark(bookmark);
  }

  getBookmark(): Bookmark {
    return this.bookmarkEditCommons.getBookmark(this.getSelectedFolders());
  }

  setAvailableFolders(folders: BookmarkFolderDocumentProperties[]): void {
    this.availableFoldersMap.clear();

    for (let folder of folders) {
      this.availableFoldersMap.set(folder.id, folder);
    }

    this.setFoldersOn(availableFoldersDivIdSelector, folders, false);
  }

  onFolderSearchText(fn: (s: string) => void) {
    ViewCommons.keyUpHandler(folderSearchKeyInputIdSelector, fn);
  }

  onBookmarkButtonClicked(fn: () => void) {
    jq(bookmarkButtonIdSelector).click(() => { fn(); })
  }

  onReindexButtonClicked(fn: () => void) {
    jq(reindexButtonIdSelector).click(() => { fn(); })
  }

  markError(field: Field): void {
    this.toggleError(field, true);
  }

  clearError(field: Field): void {
    this.toggleError(field, false);
  }

  showStatus(
    status: Status,
    errorMsg: string = "Unknown Error. Argh. Check the browser toolbox logs and let the stupid dev know."
  ) {
    switch (status) {
      case Status.ERROR:
        ViewCommons.showStatusDialog("Error", errorMsg);
        return;
      case Status.INDEX_IN_PROGRESS:
        ViewCommons.showStatusDialog("Notice", "Indexing in Progress. Try searching again in a moment. This occurs in the background when the extension is first loaded.");
        return;
      case Status.OK:
        /* nothing to do */
        return;
      default:
        Util.assertNever(status);
    }
  }

  private getSelectedFolders(): BookmarkFolderDocumentProperties[] {
    return Array.from(this.selectedFoldersMap.values());
  }

  private toggleError(field: Field, error: boolean) {
    this.bookmarkEditCommons.toggleError(field, error);

    // okay to fall through, if redundant.

    switch (field) {
      case Field.BookmarkTitle:
      case Field.BookmarkUrl:
        return; /* handled by bookmarkEditCommons call above */

      case Field.SelectedFolders:
        let e = jq(selectedFoldersDivIdSelector);
        if (error) {
          jq(selectedFoldersDivIdSelector).addClass(noSelectedFoldersErrorClass);
        } else {
          jq(selectedFoldersDivIdSelector).removeClass(noSelectedFoldersErrorClass);
        }
        return;

      default:
        Util.assertNever(field);
    }
  }

  private attachEventHandlers() {
    let view = this;

    /*
     * Basic Bookmark Fields
     */
    this.bookmarkEditCommons.attachErrorEventHandling();

    /*
     * Folder Search Input
     */
     ViewCommons.inputElementHintingHandler(folderSearchKeyInputIdSelector, searchHint);
     
    /*
     * Folder Selection Event Handlers - using Delegated Event Handling:
     *
     * Event handler on parent element, but then selecting on child elements.
     * This allows us to dynamically modify child elements without resetting 
     * the event handling on them each time.
     */
    jq(availableFoldersDivIdSelector).on('click', folderEntryClassSelector, function() {
      let id: ID = jq(this).attr(folderIdAttr) as ID;
      Util.logBookmarkDialog("Clicked", id);
      view.addSelectedFolder(id);
    })

    jq(selectedFoldersDivIdSelector).on('click', `${folderEntryClassSelector}${folderEntryRemovableClassSelector}`, function() {
      let id: ID = jq(this).attr(folderIdAttr) as ID;
      Util.logBookmarkDialog("ClickedRemovable", id);
      view.removeSelectedFolder(id);
    })
  }

  private addSelectedFolder(id: ID) {
    this.clearError(Field.SelectedFolders);

    if (this.selectedFoldersMap.has(id)) {
      // animate entries on both lists to hint already selected
      ViewCommons.animateSelection(jq(`${folderEntryClassSelector}[${folderIdAttr}=${id}]`));
      return; // nothing further to do
    }

    this.selectedFoldersMap.set(id, this.availableFoldersMap.get(id)!);
    this.refreshSelectedFolders();
  }

  private removeSelectedFolder(id: ID) {
    if (!this.selectedFoldersMap.has(id)) {
      return; // nothing to do.
    }

    this.selectedFoldersMap.delete(id)
    this.refreshSelectedFolders();
  }

  private refreshSelectedFolders() {
    this.setFoldersOn(selectedFoldersDivIdSelector, Array.from(this.selectedFoldersMap.values()), true);
  }

  private setFoldersOn(divId: string, folders: BookmarkFolderDocumentProperties[], removable: boolean): void {
    let removableClass = removable ? folderEntryRemovableClass : ''
    let toFolderEntry = (doc: BookmarkFolderDocumentProperties): string => {
      return `<div class="${folderEntryClass} ${removableClass}" ${folderIdAttr}="${doc.id}">${doc.displayTitle}</div><br />`;
    };

    let container = jq(divId);

    container.empty();

    folders.forEach((folder: BookmarkFolderDocumentProperties) => {
      container.append(toFolderEntry(folder));
    })
  }
}

