import AppConstants from 'app-libs/app-constants';
import { BookmarkFolderDocument } from 'app-libs/bookmark/bookmark-folder-document';
import { BookmarkPath } from 'app-libs/bookmark/bookmark-util';
import Util from 'app-libs/util';

export type ID = string;

export enum Field {
  BookmarkUrl,
  BookmarkTitle,
  SelectedFolders
}

export class Bookmark {
  readonly id?: string; // only available for existing bookmarks
  readonly url: string;
  readonly title: string;
  readonly selectedFolders: BookmarkFolderDocument[];

  constructor(url: string, title: string, selectedFolders: BookmarkFolderDocument[] = [], id: string | undefined = undefined) {
    this.id = id;
    this.url = url.trim();
    this.title = title.trim();
    this.selectedFolders = selectedFolders;
  }

  /**
   * Validates all fields, ignoring excludes, resultFn is called on each validated field.
   */
  validate(resultFn: (field: Field, valid: boolean) => void, excludeFields: Field[] = []): void {
    let isExcluded = (val: Field) => {
      return excludeFields.some(f => { return f === val });
    }

    for (let val of Util.enumValuesOf(Field)) {
      if (!isExcluded(val)) {
        resultFn(val, this.validateField(val));
      }
    }
  }

  validateField(field: Field): boolean {
    switch (field) {
      case (Field.BookmarkUrl):
        return Util.isEmpty(this.url) ? false : true;

      case (Field.BookmarkTitle):
        return Util.isEmpty(this.title) ? false : true;

      case (Field.SelectedFolders):
        return (this.selectedFolders.length === 0) ? false : true;

      default:
        Util.assertNever(field);
        return true; // should never get here.
    }
  }
}

export class BookmarkEntry {
  readonly displayTitle: string;

  get id(): ID {
    return this.bookmarkPath.id;
  }

  get url(): string {
    return this.bookmarkPath.url;
  }

  get title(): string {
    return this.bookmarkPath.title;
  }

  static of(paths: BookmarkPath[]): BookmarkEntry[] {
    return paths.map(p => { return new BookmarkEntry(p); });
  }
  
  constructor(private readonly bookmarkPath: BookmarkPath) {
    let maxLen = Math.min(this.url.length, AppConstants.MAX_DISPLAYABLE_URL_LENGTH);
    let trimmedUrl = maxLen === this.url.length ? this.url.substring(0, maxLen) : this.url.substring(0, maxLen - 3) + AppConstants.URL_INCOMPLETE_SUFFIX;

    let pathElems = bookmarkPath.parentFolders.map(f => { return f.title });
    pathElems.push(bookmarkPath.title);

    let displayTitle = pathElems.map((e) => {
      return e;
    }).join(AppConstants.DISPLAY_TITLE_PATH_SEPARATOR);

    this.displayTitle = `${displayTitle} ${AppConstants.URL_OPEN_MARKER}${trimmedUrl}${AppConstants.URL_CLOSE_MARKER}`;
  }

  toNewBookmark(): Bookmark {
    return new Bookmark(this.url, this.title, [], this.id);
  }
}
