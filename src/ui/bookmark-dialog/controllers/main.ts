import BookmarkToFoldersController from 'ui/bookmark-dialog/controllers/bookmark-to-folders';
import BookmarkSearchController from 'ui/bookmark-dialog/controllers/bookmark-search';

export default class BookmarkDialogMainController {
  run() {
    new BookmarkToFoldersController().run()
    new BookmarkSearchController().run()
  }
}
