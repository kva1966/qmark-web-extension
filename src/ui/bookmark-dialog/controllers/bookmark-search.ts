import AppConstants from 'app-libs/app-constants';
import { Bookmarks, BookmarkPath } from 'app-libs/bookmark/bookmark-util';
import { Bookmark, BookmarkEntry, Field } from 'ui/bookmark-dialog/model';
import BookmarkSearchView from 'ui/bookmark-dialog/view/bookmark-search';
import Util from 'app-libs/util';

export default class BookmarkSearchController {
  private static readonly MIN_SEARCH_KEY_LEN = AppConstants.DEFAULT_NGRAM_LENGTH;

  private readonly view: BookmarkSearchView;
  private lastSearchPhrase: string;

  constructor() {
    this.view = new BookmarkSearchView();
  }

  run() {
    this
      .listenToBookmarkSearchInput()
      .listenForEntryClicked();
  }

  private listenToBookmarkSearchInput(): BookmarkSearchController {
    let _this = this;

    let searchFn = (s: string) => {
      let phrase = _this.lastSearchPhrase = s.trim();
      _this.searchBookmarks(s);
    }

    let debouncedFn = Util.debounce(searchFn, AppConstants.SEARCH_INPUT_DEBOUNCE_RATE_MS, false);

    this.view.onBookmarkSearchText(debouncedFn);

    return this;
  }

  private listenForEntryClicked(): BookmarkSearchController {
    let _this = this;

    this.view.onBookmarkEntryClicked((entry: BookmarkEntry) => {
      Util.logBookmarkDialog("Entry clicked", entry);
      browser.tabs.create({ active: true, url: entry.url });
    });

    this.view.onBookmarkEntryMiddleClicked((entry: BookmarkEntry) => {
      Util.logBookmarkDialog("Entry middle clicked", entry);
      browser.tabs.create({ active: false, url: entry.url });
    });

    this.view.onBookmarkEntryShiftClicked((entry: BookmarkEntry) => {
      Util.logBookmarkDialog("Entry right clicked", entry);
      this.view.openModifyBookmarkDialog(
        entry,
        (bookmark: Bookmark) => {
          _this.updateBookmark(bookmark);
        },
        (bookmark: Bookmark) => {
          _this.deleteBookmark(bookmark);
        }
      );
    });

    return this;
  }

  private refreshSearchResults() {
    this.searchBookmarks(this.lastSearchPhrase)
  }

  private searchBookmarks(phrase: string) {
    Util.logBookmarkDialog(`Got text[${phrase}]`);

    if (phrase.length >= BookmarkSearchController.MIN_SEARCH_KEY_LEN) {
      Bookmarks.search(Util.CTX_BOOKMARK_DLG, phrase)
        .then((paths: BookmarkPath[]) => {
          this.view.setAvailableBookmarks(BookmarkEntry.of(paths));
        })
        .catch(reason => {
          let msg = `Failed to search bookmarks, reason[${reason}]`;
          Util.logBookmarkDialog(msg, reason);
          this.view.showError(msg);
        });
    } else {
      this.view.setAvailableBookmarks([]);
    }
  }

  private deleteBookmark(bookmark: Bookmark) {
    browser.bookmarks
      .remove(bookmark.id!)
      .then(() => {
        Util.logBookmarkDialog(`Successfully deleted bookmark[${bookmark.id}]`, bookmark);
        this.refreshSearchResults();
        this.view.closeModifyBookmarkDialog();
      })
      .catch((reason) => {
        let msg: string = `Failed to delete bookmark[${bookmark.id}], reason[${reason}]`;
        Util.logBookmarkDialog(msg, reason);
        this.view.showError(msg);
      });
  }

  private updateBookmark(bookmark: Bookmark) {
    let allValid: boolean = true;

    bookmark.validate((field: Field, valid: boolean) => {
      allValid = allValid && valid;

      if (!valid) {
        this.view.markBookmarkEditDialogError(field);
      }
    }, [Field.SelectedFolders] /* not updating selected folders */);

    if (allValid) {
      // Update bookmark and close popup.
      Util.logBookmarkDialog(`Updating bookmark[${bookmark.id}] details.`, bookmark);

      browser.bookmarks
        .update(bookmark.id!, { title: bookmark.title, url: bookmark.url })
        .then(node => {
          Util.logBookmarkDialog(`Successfully updated bookmark[${bookmark.id}]`, bookmark);
          this.refreshSearchResults();
          this.view.closeModifyBookmarkDialog();
        })
        .catch((reason) => {
          let msg: string = `Failed to update bookmark[${bookmark.id}], reason[${reason}]`;
          Util.logBookmarkDialog(msg, reason);
          this.view.showError(msg);
        });
    }
  }
}
