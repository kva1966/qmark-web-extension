import AppConstants from 'app-libs/app-constants';

import { BookmarkFolderDocumentProperties, BookmarkFolderDocuments } from 'app-libs/bookmark/bookmark-folder-document';
import { Status } from 'app-libs/messages';
import { MessageHandler, ResponseReceiver } from 'app-libs/folder-search/message-handler';
import Util from 'app-libs/util';

import BookmarkToFoldersView from 'ui/bookmark-dialog/view/bookmark-to-folders';
import { Bookmark, Field } from 'ui/bookmark-dialog/model';

/*
 * Interaction Model:
 *
 * if search text empty or less than ngram size -> get all
 * otherwise -> 
 *   filter by search string and criteria
 * exception: 
 *   Index in Progress status 
 *     -- display message on UI
 *     -- throttle before resending message?  
 */

export default class BookmarkToFoldersController {
  private static readonly MIN_SEARCH_KEY_LEN = AppConstants.DEFAULT_NGRAM_LENGTH;
  private readonly view: BookmarkToFoldersView;
  private readonly folderSearchHandler: MessageHandler;

  constructor() {
    this.view = new BookmarkToFoldersView();
    this.folderSearchHandler = new MessageHandler(Util.CTX_BOOKMARK_DLG, {
      onStatus: (status: Status, msg?: string): void => {
        this.view.showStatus(status, msg)
      },

      onFolderResults: (folders: BookmarkFolderDocumentProperties[]): void => {
        this.view.setAvailableFolders(folders)
      }
    });
  }

  run() {
    this
      .initFolderSearchMessaging()
      .populateBookmarkFromActiveTab()
      .initFolders()
      .listenToFolderSearchInput()
      .listenToBookmarkButton()
      .listenToReindexButton();
  }

  private initFolderSearchMessaging(): BookmarkToFoldersController {
    this.folderSearchHandler.init();

    return this;
  }

  private listenToFolderSearchInput(): BookmarkToFoldersController {
    let _this = this;

    let searchFn = (s: string) => {
      _this.folderSearchHandler.searchFor(s);
    }

    let debouncedFn = Util.debounce(searchFn, AppConstants.SEARCH_INPUT_DEBOUNCE_RATE_MS, false);

    this.view.onFolderSearchText(debouncedFn);

    return this;
  }

  private listenToReindexButton(): BookmarkToFoldersController {
    this.view.onReindexButtonClicked(() => {
      this.folderSearchHandler.issueFullReindexRequest();
    });

    return this;
  }

  private listenToBookmarkButton(): BookmarkToFoldersController {
    this.view.onBookmarkButtonClicked(() => {
      let bookmark: Bookmark = this.view.getBookmark();
      let allValid: boolean = true;

      // validate
      bookmark.validate((field: Field, valid: boolean) => {
        allValid = allValid && valid;

        if (!valid) {
          this.view.markError(field);
        }
      });

      if (allValid) {
        // create bookmark and close popup.
        Util.logBookmarkDialog("Creating bookmark in selected folders");

        BookmarkFolderDocuments.create(
          bookmark.url,
          bookmark.title,
          bookmark.selectedFolders
        ).then(
          /* success */
          (nodes: browser.bookmarks.BookmarkTreeNode[]) => {
            window.close();
          }
        ).catch(
          /* failure */
          (reason) => {
            let msg: string = `Failed to create one or more bookmarks[${reason}]`;
            Util.logBookmarkDialog(msg, reason);
            this.view.showStatus(Status.ERROR, msg);
          }
        );
      }
    });

    return this;
  }

  private populateBookmarkFromActiveTab(): BookmarkToFoldersController {
    Util.queryActiveTab((tab: browser.tabs.Tab) => {    
      Util.logBookmarkDialog(`Found active tab, URL[${tab.url}]`);
      if (tab) {
        this.view.setBookmark(new Bookmark(tab.url, tab.title));
      }
    });

    return this;
  }

  private initFolders(): BookmarkToFoldersController {
    this.folderSearchHandler.requestAllFolders();
    
    return this;
  }
}
