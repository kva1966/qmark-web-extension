// Vendor Libs: Webpack is Node Modules Dependencies-aware
// jquery requires some special handling.
require('jquery');
require('jquery-ui-dist/jquery-ui');

import * as jQuery from 'jquery';
const jq = jQuery;

// Run top-level controller
import BookmarkDialogMainController from 'ui/bookmark-dialog/controllers/main';
import UTIL from 'app-libs/util';

// Could put this logic in index.hbs, but FF extension mechanism triggers a 
// security error
jq(document).ready(() => {
  new BookmarkDialogMainController().run();
  $('#tabs-container').tabs();
});

UTIL.logBookmarkDialog("Loaded");
